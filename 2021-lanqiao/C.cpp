#include <bits/stdc++.h>
using namespace std;
#define LLUI long long int

class CbigInt
{
private:
    // char *p;
    char p[1000];

public:
    CbigInt() {}
    ~CbigInt() {} // 析构函数
    friend istream &operator>>(istream &, CbigInt &);
    friend ostream &operator<<(ostream &, CbigInt &);
    CbigInt operator+(const CbigInt &num2)
    {
        CbigInt ans, tempAns;
        int length1, length2, length3;
        bool next = 0;

        length1 = strlen(this->p);
        length2 = strlen(num2.p);
        if (length1 == length2)
            length3 = length1;
        else
            length3 = (length1 > length2 ? length1 : length2);

        int i = length1 - 1, j = length2 - 1, k = 0;

        while (i >= 0 && j >= 0) // 共有位
            tempAns.p[k++] = this->p[i--] + num2.p[j--] - '0';
        while (i >= 0) // this
            tempAns.p[k++] = this->p[i--];
        while (j >= 0) // num2
            tempAns.p[k++] = num2.p[j--];
        tempAns.p[k] = '\0';

        // process 进位
        k = 0;
        while (k < length3)
        {
            // cout << tempAns.p[k] << endl;
            if (true == next)
            {
                tempAns.p[k]++;
                next = false;
            }
            if (tempAns.p[k] > '9')
            {
                tempAns.p[k] -= 10;
                next = true;
            }
            k++;
        }
        //
        if (true == next)
            tempAns.p[k] = '1', tempAns.p[k + 1] = '\0';
        // 逆置
        for (int i = 0; i < strlen(tempAns.p); i++)
            ans.p[i] = tempAns.p[strlen(tempAns.p) - 1 - i];
        ans.p[strlen(tempAns.p)] = '\0';

        return ans;
    }
    CbigInt operator-(const CbigInt &num2)
    {
        CbigInt temp, ans, tempAns;
        int length1, length2;
        temp = num2;

        length1 = strlen(this->p);
        length2 = strlen(num2.p);

        int i = length1 - 1, j = length2 - 1, k = 0;

        if (length1 > length2)
        {
            while (j >= 0)
            {
                if (this->p[i] < num2.p[j])
                {
                    this->p[i - 1] -= 1;
                    this->p[i] += 10;
                }
                tempAns.p[k++] = this->p[i--] - num2.p[j--] + '0';
            }
            while (i >= 0)
            {
                if (this->p[i] < '0')
                {
                    this->p[i - 1] -= 1;
                    this->p[i] += 10;
                }
                tempAns.p[k++] = this->p[i--];
            }
            tempAns.p[k] = '\0';
        }
        else if (length2 > length1)
        {
            while (i >= 0)
            {
                if (this->p[i] > num2.p[j])
                {
                    temp.p[j - 1] -= 1;
                    temp.p[j] += 10;
                }
                tempAns.p[k++] = temp.p[j--] - this->p[i--] + '0';
            }
            while (j >= 0)
            {
                if (num2.p[j] < '0')
                {
                    temp.p[j - 1] -= 1;
                    temp.p[j] += 10;
                }
                tempAns.p[k++] = temp.p[j--];
            }
            tempAns.p[k++] = '-';
            tempAns.p[k++] = '\0';
        }
        else
        {
            int b = 0;
            bool flag = false;
            while (1)
            {
                if (this->p[b] > num2.p[b])
                    break;
                else if (this->p[b] < num2.p[b])
                {
                    flag = true;
                    break;
                }
                else
                    b++;
            }
            if (false == flag) // 减数大
            {
                while (j >= 0)
                {
                    if (temp.p[i] < num2.p[j]) // 借位
                    {
                        this->p[i - 1] -= 1;
                        this->p[i] += 10;
                    }
                    tempAns.p[k++] = this->p[i--] - num2.p[j--] + '0';
                }
            }
            else // 减数小
            {
                while (i >= 0)
                {
                    if (temp.p[j] < this->p[i])
                    {
                        temp.p[j - 1] -= 1;
                        temp.p[j += 10];
                    }
                    tempAns.p[k++] = temp.p[j--] - this->p[i--] + '0';
                }
                tempAns.p[k++] = '-';
                tempAns.p[k++] = '\0';
            }
        }

        // 逆置
        for (int i = 0; i < strlen(tempAns.p); i++)
            ans.p[i] = tempAns.p[strlen(tempAns.p) - 1 - i];
        ans.p[strlen(tempAns.p)] = '\0';

        return ans;
    }
};

istream &operator>>(istream &input, CbigInt &s)
{
    input >> s.p;
    return input;
}
ostream &operator<<(ostream &output, CbigInt &s)
{
    output << s.p;
    return output;
}

LLUI C(int n, int m)
{
    LLUI count = 1;
    if (m == 0 || m == n)
        return 1;
    for (int i = n; i >= n - m + 1; i--)
    {
        count *= i;
        if (count > 4294967)
            count %= 13331;
    }
    for (int i = m; i > 0; i--)
    {
        count /= i;
        if (count > 4294967)
            count %= 13331;
    }
    return count;
}

int main()
{
    freopen("../dataC.in", "r", stdin);
    // freopen("../data1.out", "w", stdout);
    int n, m, p;
    LLUI count = 0;
    cin >> n >> m;
    cin >> p;
    vector<int> A;
    vector<int> B;

    for (int i = 0; i <= n; i++)
        A.push_back(C(n, i));
    for (int i = 0; i <= m; i++)
        B.push_back(C(m, i));

    for (int i = 0; i <= n; i++)     // A
        for (int j = 0; j <= m; j++) // B
        {
            if (i + j == 0 || i + j > p)
                continue;
            else
                count += A[i] * B[j];
        }
    count %= 13331;
    printf("%lld", count);

    return 0;
}