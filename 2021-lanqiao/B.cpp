#include <bits/stdc++.h>
using namespace std;
#define LLUI long long int

LLUI n, Q;

struct Nuke
{
    LLUI x = 0;
    LLUI v = 0;
};

Nuke N[100002];
LLUI k[100002];

LLUI Launch(Nuke *N)
{
    LLUI E = 0; // Energy
    LLUI last = 0;
    for (LLUI i = 0; i < n; i++) // i=pos
    {
        E = E - (N[i].x - last) + N[i].v; // Explosive
        last = N[i].x;                    // Mark
        if (E - N[i].v < 0)               // Force to next site but negative Energy
            return N[i].x + (E - N[i].v); // farest
    }
    return last + E;
}

int main()
{
    // freopen("../dataB.in", "r", stdin);
    // freopen("../data1.out", "w", stdout);
    LLUI reach = 0;

    scanf("%d", &n);
    for (LLUI i = 0; i < n; i++)
    {
        scanf("%lld", &N[i].x);
        scanf("%lld", &N[i].v);
    }
    scanf("%d", &Q);
    for (LLUI i = 0; i < Q; i++)
        scanf("%lld", &k[i]);

    // process
    reach = Launch(N);
    // cout << "Reach = " << reach << endl;

    for (LLUI i = 0; i < Q; i++)
        if (k[i] <= reach)
            printf("Yes\n");
        else
            printf("No\n");
    return 0;
}