#include <bits/stdc++.h>
using namespace std;
int n, m;

void Show(vector<vector<char>> &mat)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
            cout << mat[i][j];
        cout << endl;
    }
}
int main()
{
    // freopen("../dataA.in", "r", stdin);
    // freopen("../data1.out", "w", stdout);

    int Vx, Vy;
    int Ex, Ey;
    int count = 0;
    cin >> n >> m;
    vector<vector<char>> mat(n, vector<char>(m));
    for (int i = 0; i < n; i++)
        for (int j = 0; j < m; j++)
        {
            cin >> mat[i][j];
            if (mat[i][j] == 'E')
                Ex = i, Ey = j;
            if (mat[i][j] == 'V')
                Vx = i, Vy = j;
        }

    int x1 = Vx, y1 = Vy;
    int x2 = Vx, y2 = Vy;
    while (mat[Ex][Ey] != 'V')
    {
        x1 -= 1, y1 -= 1;
        x2 += 1, y2 += 1;
        for (int i = x1; i <= x2 && i < n; i++)
            for (int j = y1; j <= y2 && j < m; j++)
            {
                if (i < 0)
                    i = 0;
                if (j < 0)
                    j = 0;
                mat[i][j] = 'V';
            }
        // Show(mat);
        count += 1;
        // cout << endl;
    }
    cout << count << endl;

    return 0;
}