#include <bits/stdc++.h>
using namespace std;
#define LLUI long long int

int n, Q;

struct Nuke
{
    LLUI x = 0;
    LLUI v = 0;
};

Nuke N[100002];
LLUI k[100002];

void Query(Nuke *N, LLUI &k)
{
    LLUI E = 0;
    int indexN = 0;
    for (LLUI i = 0; i <= k; i++) //pos = i
    {
        if (N[indexN].x == i)
        {
            E += N[indexN].v;
            indexN += 1;
        }
        if (k <= i)
        {
            cout << "Yes" << endl;
            break;
        }
        else if (E <= 0 && i < k)
        {
            cout << "No" << endl;
            break;
        }
        E -= 1; //exhaust
    }
    cout << "No" << endl;
}

int main()
{
    freopen("../dataB.in", "r", stdin);
    // freopen("../data1.out", "w", stdout);
    memset(N, 0, sizeof(N));
    LLUI temp;
    Nuke nTemp;

    cin >> n;
    for (LLUI i = 0; i < n; i++)
    {
        cin >> N[i].x;
        cin >> N[i].v;
    }
    cin >> Q;
    for (LLUI i = 0; i < Q; i++)
    {
        cin >> k[i];
    }

    // process
    for (LLUI i = 0; i < Q; i++)
        Query(N, k[i]);

    return 0;
}