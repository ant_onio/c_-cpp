n = input()
a = list(input().split())
# a = [1, 1, 2, 3, 3, 5, 6, 15]
for i in range(len(a)):
  a[i] = int(a[i])
a = list(set(a))
# print(a)

ans = []
b = []

for i in reversed(a):
    # print(i)
    j = 1
    while(j <= i/2):
        if i % j == 0:
            b.insert(len(b), j)
            b.insert(len(b), int(i/j))
            # print(b)
        j += 1
    ans.insert(len(ans), i)
    b = list(set(b))
    if len(b) == len(a):
        break
ans = list(reversed(ans))
print(len(ans))
print(*ans, sep=" ")