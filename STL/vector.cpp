#include <bits/stdc++.h>
using namespace std;

int main()
{
    // define
    vector<int> v; //声明
    printf("size before pushback:%d\n", v.size());
    v.push_back(1);
    printf("%d\n", v[0]);
    printf("size after pushback:%d\n", v.size());

    v.pop_back();
    printf("size after pop_back:%d\n", v.size());
    return 0;
}