# 判断
vec.empty()

# 查看
vec.size()  // 大小

# 操作
``` CPP

vec.pop_back()  // 删除最后一项
vec.back() // 取最后一个元素
vec.clear() // 清空
```