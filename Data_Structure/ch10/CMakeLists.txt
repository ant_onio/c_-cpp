cmake_minimum_required(VERSION 3.0)

project(ch9)

SET(CMAKE_BUILD_TYPE "Debug")
SET(CMAKE_CXX_FLAGS_DEBUG "$ENV{CXXFLAGS} -O0 -Wall -g2 -ggdb")  
SET(CMAKE_CXX_FLAGS_RELEASE "$ENV{CXXFLAGS} -O3 -Wall")  


include_directories(${CMAKE_SOURCE_DIR}/include)
link_directories(./lib)

add_compile_options(-Wall -std=c++11)

add_executable(Exec1 exec1.cpp)
add_executable(FUN fun.cpp)
# add_executable(Exec2 exec2.cpp)
# add_executable(Exec3 exec3.cpp)