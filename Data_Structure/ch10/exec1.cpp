#include <bits/stdc++.h>
using namespace std;

class X
{
public:
    int x[100000];
};

void Show(X R, int n)
{
    for (int i = 0; i < n - 1; i++)
        cout << R.x[i] << " ";
    cout << R.x[n - 1] << endl;
}

void BiInsertionSort(X R, int n)
{
    int m = 0, low = 0, high = n - 1;
    int temp;
    for (int i = 1; i < n; i++)
    {
        temp = R.x[i];
        low = 0, high = i;
        while (low < high)
        {
            m = (low + high) / 2;
            if (temp < R.x[m])
                high = m - 1;
            else
                low = m + 1;
        }
        for (int j = i - 1; j > high + 1; --j)
            R.x[j + 1] = R.x[j];
        R.x[high + 1] = temp;
    }

    Show(R, n);
}
void BubbleSort(X R, int n)
{
    int temp = 0;
    for (int i = 0; i < n - 1; i++)
        for (int j = i; j < n; j++)
            if (R.x[i] > R.x[j])
            {
                temp = R.x[i];
                R.x[i] = R.x[j];
                R.x[j] = temp;
            }
    Show(R, n);
}

void Qsort(X R, int n, int s, int t, bool show)
{
    int pivotloc;
    if (s < t)
        pivotloc = 0;
    if (show)
        Show(R, n);
}

void SelectSort(X R, int n)
{
    int temp = 0, min = 0;
    for (int i = 0; i < n - 1; i++)
    {
        min = i;
        for (int j = i + 1; j < n; j++)
            if (R.x[min] > R.x[j])
                min = j;
        temp = R.x[i];
        R.x[i] = R.x[min];
        R.x[min] = temp;
    }
    Show(R, n);
}
void MSort() {}
void MergeSort(X R, int n)
{
    // MSort(R, R, 1, n);
    Show(R, n);
}
void HeapSort(X R, int n)
{
    Show(R, n);
}

X a;

int main()
{
    freopen("../data1.txt", "r", stdin);
    freopen("../data1.out", "w", stdout);
    int n, temp;
    cin >> n;
    // int x[n];
    for (int i = 0; i < n; i++)
        cin >> a.x[i];

    BiInsertionSort(a, n);    // 1-BiInsertion
    BubbleSort(a, n);         // 2-Bubble
    Qsort(a, n, 0, n - 1, 1); // 3-quickSort
    SelectSort(a, n);         // 4-Select
    MergeSort(a, n);          // 5-Merge
    HeapSort(a, n);           // 6-Heap

    return 0;
}