#include <bits/stdc++.h>
using namespace std;
int cmpfunc(const void *a, const void *b) { return (*(int *)a - *(int *)b); }
int main()
{
    int n;
    cin >> n;
    int x[n];
    for (int i = 0; i < n; i++)
    {
        cin >> x[i];
    }
    qsort(x, n, sizeof(int), cmpfunc);
    for (int j = 0; j < 6; j++)
    {
        for (int i = 0; i < n - 1; i++)
        {
            cout << x[i] << " ";
        }
        cout << x[n - 1] << endl;
    }
}