/*
 * 共有n个车位
 * 最多可允许m辆车排队等待
 * 共有q次操作
 * lot - queue
 * move - queue
 * wait - queue
 */

#include <iostream>
using namespace std;

struct operation
{
    int opID, x;
};

struct queue
{
    int num;
    struct queue *next = NULL;
};

int n, m, q;

void Push(queue *&top, int x)
{
    queue *e = new queue;
    e->num = x;
    e->next = top;
    top = e;
}
void Enqueue(queue *&rear, int x)
{
    queue *e = new queue;
    e->num = x;
    rear->next = e;
    rear = e;
}
void Dequeue(queue *&front, queue *&rear)
{
    queue *cur = front->next;
    if (cur == rear)
        rear = front;
    front->next = cur->next;
    delete cur;
}
void Pop(queue *&top)
{
    queue *cur = top;
    top = cur->next;
    delete cur;
}
int Length(queue *&top)
{
    queue *cur = top;
    int len = 0;
    while (NULL != cur->next)
    {
        cur = cur->next;
        len += 1;
    }
    return len;
}
void Display(queue *&top)
{
    queue *cur = top;
    queue *DP = new queue;
    while (cur->next != NULL)
    {
        Push(DP, cur->num);
        cur = cur->next;
    }
    while (DP->next != NULL)
    {
        cout << " " << DP->num;
        Pop(DP);
    }
    cout << endl;
    delete DP;
}
void DisplayQ(queue *&top)
{
    queue *cur = top->next;
    // queue *DP = new queue;
    while (cur != NULL)
    {
        cout << " " << cur->num;
        cur = cur->next;
    }
    cout << endl;
}
int find(queue *&top, int e)
{
    if (Length(top) < e || e == 0)
        return -1;
    else
    {
        queue *cur = top;
        queue *f = new queue;
        while (cur->next != NULL)
        {
            Push(f, cur->num);
            cur = cur->next;
        }
        for (int i = 1; i < e; i++)
            f = f->next;
        return f->num;
    }
}
int findQ(queue *&top, int e)
{
    if (Length(top) < e || e == 0)
        return -1;
    else
    {
        queue *cur = top->next;
        for (int i = 1; i < e; i++)
            cur = cur->next;
        return cur->num;
    }
}

int main()
{
    // freopen("../data1.txt", "r", stdin);
    // freopen("../data1out.txt", "w", stdout);
    cin >> n >> m >> q;
    operation op[q]; // op log
    queue *lotTop;
    queue *moveTop;
    queue *waitFront, *waitRear;
    lotTop = new queue;
    moveTop = new queue;
    waitFront = new queue, waitRear = waitFront;

    // input op
    for (int i = 0; i < q; i++)
        cin >> op[i].opID >> op[i].x;

    // process
    for (int i = 0; i < q; i++)
    {
        // cout << "op- " << i << endl;

        switch (op[i].opID)
        {
        case 1:                     // car in
            if (Length(lotTop) < n) // lot available
                Push(lotTop, op[i].x);
            else
            {
                if (Length(waitFront) < m)
                    Enqueue(waitRear, op[i].x); // in wait queue
            }

            cout << "parking lot:"; //display as stack
            Display(lotTop);
            cout << "waiting queue:";
            DisplayQ(waitFront);
            break;
        case 2: // plate x car out
            while (Length(lotTop) > 0)
            {
                if (lotTop->num == op[i].x)
                {
                    Pop(lotTop);
                    // moveback
                    while (Length(moveTop) > 0)
                    {
                        Push(lotTop, moveTop->num); // Push函数需修改
                        Pop(moveTop);
                    }
                    if (waitFront->next != NULL)
                    {
                        Push(lotTop, waitFront->next->num);
                        Dequeue(waitFront, waitRear);
                    }
                    break;
                }
                else
                {
                    Push(moveTop, lotTop->num);
                    Pop(lotTop);
                }
            }
            cout << "parking lot:"; //display as stack
            Display(lotTop);
            cout << "waiting queue:";
            DisplayQ(waitFront);
            break;
        case 3: // find posi x in lot
            cout << find(lotTop, op[i].x) << endl;
            break;
        case 4: // find posi x in queue
            cout << findQ(waitFront, op[i].x) << endl;
            break;
        default:
            break;
        }
    }

    return 0;
}