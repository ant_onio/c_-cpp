#include <iostream>
#include <cstring>
using namespace std;

struct sig
{
    char c;
    struct sig *next = NULL;
};
bool stackEmpty(sig *&top)
{
    return NULL == top->next ? 1 : 0;
}
void Push(sig *&top, char c)
{
    sig *e = new sig;
    e->c = c;
    e->next = top;
    top = e;
}
void Pop(sig *&top)
{
    sig *temp = top;
    top = top->next;
    delete temp;
}
sig GetTop(sig *&top)
{
    return *top;
}
int Length(sig *&top)
{
    sig *cur = top;
    int len = 0;
    while (NULL != cur->next)
    {
        cur = cur->next;
        len += 1;
    }
    return len;
}
int main()
{
    // init
    sig *top = new sig;
    sig *ltop = new sig;
    // sig *rtop = new sig;
    bool status = 1;
    char *exp;
    exp = new char[20];
    strcpy(exp, "(())[{{}}");

    for (int i = strlen(exp); i >= 0; i--)
        if (exp[i] == '(' || exp[i] == '[' || exp[i] == '{' || exp[i] == ')' || exp[i] == ']' || exp[i] == '}')
            Push(top, exp[i]);

    // match
    for (int i = 0; NULL != top->next && status; i++)
    {
        switch (top->c)
        {
        case ('('):
            Push(ltop, top->c), Pop(top);
            break;
        case ('['):
            Push(ltop, top->c), Pop(top);
            break;
        case ('{'):
            Push(ltop, top->c), Pop(top);
            break;
        case ')':
            if (GetTop(ltop).c == '(')
                Pop(ltop), Pop(top);
            else
                status = 0;
            break;
        case ']':
            if (GetTop(ltop).c == '[')
                Pop(ltop), Pop(top);
            else
                status = 0;
            break;
        case '}':
            if (GetTop(ltop).c == '{')
                Pop(ltop), Pop(top);
            else
                status = 0;
            break;
        default:
            break;
        }
    }
    if (0 != Length(ltop))
        status = false;

    cout << (status ? "sig correct" : "sig error") << endl;
    return 0;
}