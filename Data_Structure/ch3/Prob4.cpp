/*
 * 输入：一行两个数，空格分隔
 * 第一个-操作：退出0、客户领号1、业务完成2
 * 第二个-类型：领号：（公积金1、银行卡2、理财卡3）；完成：（窗口编号）
 * 输出：一行六个数，空格分隔，代表窗口办理的业务类型
 * 空闲0、公积金1、银行卡2、理财卡3
**/
#include <iostream>
// #include <cstring>
using namespace std;
struct Cus
{
    unsigned int num = 1000;
    unsigned int type = 0;
    struct Cus *next = NULL;
};

// op Function
void Enqueqe(/*Cus *&front, */ Cus *&rear, unsigned int num, unsigned int type)
{
    Cus *e = new Cus;
    e->num = num;
    e->type = type; // 业务类型

    rear->next = e;
    rear = e;
}
void Dequeue(Cus *&front, Cus *&rear)
{
    Cus *cur = front->next;
    if (cur == rear)
        rear = front;
    front->next = cur->next;
    delete cur;
}
void Display(int win[])
{
    cout << "window:";
    for (int i = 0; i < 6; i++)
        cout << win[i] << ", ";
    cout << endl;
    // cout << "queue:";
    // while
}
/**
 * @description: match input and output
 * @param {*&LNode} L, {int} i, {double} e
 * @return {*}
 */
int Match(int win)
{
    if (win == 1) //HPF
        return 1;
    else if (win >= 2 && win <= 4) // Debit
        return 2;
    else if (win >= 5 && win <= 6) // Fin
        return 3;
    else
        return 0;
}
// check Function
bool QEmpty(Cus *&front)
{
    bool empty = true;
    Cus *cur = front;
    if (cur->next != NULL)
        empty = false;
    return empty;
}

int main()
{
    freopen("../data4.txt", "r", stdin);
    // init
    int win[6] = {0}; // 窗口状态
    int count = 0;    // 计数
    bool EN_qb;       // q可用状态：0-qa;1-qb
    // Cus *q[2];               // 未启用(A+B)
    Cus *qa_front, *qa_rear; // 队伍A
    Cus *qb_front, *qb_rear; // 队伍B
    Cus *cur = NULL;
    int type = 0, op = 0; // 输入值
    bool state = true;    //true:继续；false:退出
    qa_front = new Cus, qb_front = new Cus;
    qa_rear = qa_front, qb_rear = qb_front;

    while (state)
    {
        cin >> op >> type;
        switch (op) // op process
        {
        case 0:            // 退出
            state = false; // exit
            break;
        case 1:                   // 取号
                                  // if (!EN_qb)
            if (QEmpty(qb_front)) // init state & EN_qa state
                Enqueqe(qa_rear, ++count + 1000, type);
            else if (QEmpty(qa_front))
                // else if (EN_qb)
                Enqueqe(qb_rear, ++count + 1000, type);
            else
            {
                // Err
            }
            break;
        case 2: // 办理完成
            win[type - 1] = 0;

            if (type == 1) // HPF
                type = 1;  // HPF
            else if (type >= 2 && type <= 4)
                type = 2; // Debit
            else if (type >= 5 && type <= 6)
                type = 3; // Fin
            else
                break;
            break;
        default:
            break;
        }
        // queue process
        // 最多有一个队列非空
        for (int i = 0; i < 6; i++)
        {
            if (win[i] == 0) // window available
            {
                if (QEmpty(qb_front)) // qb空、qa使能
                {
                    while (!QEmpty(qa_front)) // 清空qa
                    {
                        if (qa_front->next->type == Match(i + 1)) // 是该窗口
                        {
                            if (!QEmpty(qa_front))
                                win[i] = qa_front->next->num;
                            Dequeue(qa_front, qa_rear);
                        }
                        else
                        {
                            if (Match(i + 1) == 2 && (win[4] != 0 && win[5] != 0 && qa_front->next->type == 3)) // 银行卡窗口、理财窗口满、理财业务
                            {
                                if (qa_front->next->type == 3) // 理财业务
                                    if (Match(i + 1) == 2)     // 银行卡窗口
                                    {
                                        win[i] = qa_front->next->num;
                                        Dequeue(qa_front, qa_rear);
                                    }
                            }
                            else
                            {
                                Enqueqe(qb_rear, qa_front->next->num, qa_front->next->type);
                                Dequeue(qa_front, qa_rear);
                            }
                        }
                    }
                    // EN_qb = true; //使能qb
                }
                else // qb使能
                {
                    while (!QEmpty(qb_front)) // 清空qb
                    {
                        if (qb_front->next->type == Match(i + 1)) // 是该窗口
                        {
                            win[i] = qb_front->next->num;
                            Dequeue(qb_front, qb_rear);
                        }

                        else /*if (qb_front->next->type != Match(i + 1))*/
                        {
                            if (Match(i + 1) == 2 && (win[4] != 0 && win[5] != 0 && qb_front->next->type == 3)) // 银行卡窗口、理财窗口满、理财业务
                            {
                                if (qb_front->next->type == 3) // 理财业务
                                    if (Match(i + 1) == 2)     // 银行卡窗口
                                    {
                                        win[i] = qb_front->next->num;
                                        Dequeue(qb_front, qb_rear);
                                    }
                            }
                            else
                            {
                                Enqueqe(qa_rear, qb_front->next->num, qb_front->next->type);
                                Dequeue(qb_front, qb_rear);
                            }
                        }
                    }
                }
            }
        }

        // display
        if (state)
            Display(win);
    }
    return 0;
}