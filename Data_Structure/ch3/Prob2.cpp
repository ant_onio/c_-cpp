#include <iostream>
using namespace std;

struct qnode
{
    int f;
    struct qnode *next = NULL;
};

int main()
{
    // init
    qnode *front, *rear, *cur;
    qnode q[4];
    front = &q[0], rear = &q[3];
    for (int i = 0; i < 3; i++)
        q[i].next = &q[i + 1];
    q[3].next = &q[0];
    q[0].f = 0, q[1].f = 0, q[2].f = 0, q[3].f = 1;
    int count = 4;
    // creat
    while (rear->f < 200)
    {
        cur = front;
        for (int i = 0; i < 3; i++)
        {
            cur = cur->next;
            front->f += cur->f;
        }
        rear = front;        // 更新指针
        front = front->next; // 更新指针
        count++;             // 计数
    }
    cout << "f" << count << " = " << rear->f << endl;

    return 0;
}