#include <iostream>
using namespace std;

struct LNode
{
    int index;
    int pass;
    struct LNode *next = NULL;
};

/**
 * @description: Create new node with certain element and insert to list
 * @param {*&LNode} L, {int} i, {int} e
 * @return {*}
 */
void ListInsert(LNode *&L, int i, int e) //Head, num, element
{
    LNode *cur = L;
    // new a space
    LNode *s = new LNode;
    s->pass = e;
    s->index = i + 1;
    // goto certain place
    for (int j = 1; j < i; j++)
        cur = cur->next;
    // get linked
    (NULL == cur ? L : cur->next) = s;
} // ListInsert

/**
 * @description: Output to test List
 * @param {*&LNode} L, 
 * @return {*}
 */
void ListTest(LNode *&L)
{
    LNode *cur = L;
    while (NULL != cur)
    {
        cout << cur->pass << endl;
        cur = cur->next;
    }
    cout << endl;
}

/**
 * @description: Report and delete element
 * @param {*& LNode} L, {int} m
 * @return {*}
 */
void ListReport(LNode *&L, int m) //Head, num, element
{
    int i = 1;
    LNode *cur = L;
    LNode *toDel = L;
    while (NULL != L)
    {
        if (NULL == cur)
            cur = L;
        if (i != m)
        {
            cur = (NULL != cur->next ? cur->next : L);
            i++;
        }
        else //report & delete node
        {
            // report
            cout << cur->index << endl;
            m = cur->pass;
            // del (toDel is previous one of cur)
            while (toDel->next != cur)
            {
                if (NULL == L->next)
                    break;
                if (NULL == toDel->next)
                    toDel = L;
                else
                    toDel = toDel->next;
            }
            if (NULL == L->next)
                L = NULL;
            else
            {
                if (NULL != L->next)
                    toDel->next = toDel->next->next;
                else
                    delete cur;
            }
            cur = toDel->next;
            // post
            i = 1;
        }
    }
} // ListReport

/**
 * @description: Destroy List
 * @param {*&LNode} L
 * @return {*}
 */
void DestroyList(LNode *&L)
{
    if (NULL != L->next)
    {
        DestroyList(L->next);
        L->next = NULL;
    }
    delete L;
}

int main()
{
    freopen("../data4.txt", "r", stdin); // comment this line if you want to input data manually

    int num = 0, temp = 0, m = 0;
    LNode *head = NULL;

    cout << "Input number of data:";
    cin >> num;

    // Create List
    cout << "Input data in order:";
    for (int i = 0; i < num; i++)
    {
        scanf("%d", &temp);
        ListInsert(head, i, temp); // list, posi, element
    }
    // Test
    // ListTest(head);

    cout << "Input value of m:";
    cin >> m;

    // Report List
    ListReport(head, m); //List, m value, curent reporting person(from 1)
    // release nodes
    DestroyList(head);

    return 0;
}