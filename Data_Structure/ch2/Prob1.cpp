/* 
 * 2.19
 */
#include <iostream>
using namespace std;

struct LNode
{
    double value;
    struct LNode *next = NULL;
};

/**
 * @description: Create new node with certain element and insert to list
 * @param {*&LNode} L, {int} i, {double} e
 * @return {*}
 */
void ListInsert(LNode *&L, int i, double e) //Head, position, element
{
    LNode *cur = L;
    // new a space
    LNode *s = new LNode;
    s->value = e;
    // goto position
    for (int j = 1; j < i; j++)
        cur = cur->next;
    // get linked
    cur->next = s;
} // ListInsert

/**
 * @description: Output to test List
 * @param {*&LNode} L, 
 * @return {*}
 */
void ListTest(LNode *&L)
{
    LNode *cur = L->next;
    while (NULL != cur)
    {
        cout << cur->value <<" ";
        cur = cur->next;
    }
    cout << endl;
} // ListTest

/**
 * @description: Delete nodes
 * @param {*&LNode} L, {double} mink, {double} maxk
 * @return {*}
 */
void ListDelete(LNode *&L, double mink, double maxk)
{
    LNode *cur = L;
    LNode *del = NULL;
    while (cur->next->value <= mink)
        cur = cur->next;
    while (cur->next->value < maxk)
    {
        del = cur->next;
        cur->next = cur->next->next;
        delete del;
    }
} //ListDelete

/**
 * @description: Destroy List
 * @param {*&LNode} L
 * @return {*}
 */
void DestroyList(LNode *&L)
{
    if (NULL != L->next)
    {
        DestroyList(L->next);
        L->next = NULL;
    }
    delete L;
}

int main()
{
    freopen("../data1.txt", "r", stdin); // comment this line if you want to input data manually
    int num = 0;
    double temp = 0, mink = 0, maxk = 0;
    LNode *head = new LNode;

    cout << "Input number of data:";
    cin >> num;

    // Create List
    cout << "Input data in order:";
    for (int i = 1; i <= num; i++)
    {
        scanf("%lf", &temp);
        ListInsert(head, i, temp); // list, posi, element
    }

    cout << "Input value of mink, maxk:";
    cin >> mink >> maxk;

    // Process
    cout << "before:" << endl;
    ListTest(head);

    ListDelete(head, mink, maxk);

    cout << "after:" << endl;
    ListTest(head);

    // release nodes
    DestroyList(head);

    return 0;
}