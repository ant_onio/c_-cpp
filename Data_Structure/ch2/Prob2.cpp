/**
 *  2.22
 */
#include <iostream>
using namespace std;

struct LNode
{
    int data;
    struct LNode *next = NULL;
};

/**
 * @description: Create new node with certain element and insert to list
 * @param {*&LNode} L, {int} i, {int} e
 * @return {*}
 */
void ListInsert(LNode *&L, int i, int e) //Head, position, element
{
    LNode *cur = L;
    // new a space
    LNode *s = new LNode;
    s->data = e;
    // goto position
    for (int j = 1; j < i; j++)
        cur = cur->next;
    // get linked
    cur->next = s;
} // ListInsert

/**
 * @description: Insert Node
 * @param {*&LNode} Node, {*&LNode} L, {int} posi
 * @return {*}
 */
void NodeInsert(LNode *&Node, LNode *&L, int posi)
{
    LNode *cur = L;
    // goto
    for (int i = 1; i < posi; i++)
        cur = cur->next;
    // link
    Node->next = cur->next; //back
    cur->next = Node;       //forward
} // NodeInsert

/**
 * @description: Output to test List
 * @param {*&LNode} L, 
 * @return {*}
 */
void ListTest(LNode *&L)
{
    LNode *cur = L->next;
    while (NULL != cur)
    {
        cout << cur->data << endl;
        cur = cur->next;
    }
    cout << endl;
} // ListTest

void ListRevert(LNode *&L) // 头插法
{
    /*
    1. 断Head与data
    2. data的头始终向head的1插入
    */
    LNode *cur = L->next;
    LNode *next = cur->next;
    L->next = NULL; //cut head
    while (NULL != cur)
    {
        NodeInsert(cur, L, 1);
        cur = next;
        next = (NULL == next ? NULL : next->next);
    }
} // ListRevert

/**
 * @description: Destroy List
 * @param {*&LNode} L
 * @return {*}
 */
void DestroyList(LNode *&L)
{
    if (NULL != L->next)
    {
        DestroyList(L->next);
        L->next = NULL;
    }
    delete L;
}

int main()
{
    freopen("../data2.txt", "r", stdin); // comment this line if you want to input data manually
    int num = 0, temp = 0;
    LNode *head = new LNode;
    cin >> num;

    // Create List
    cout << "Input data in order:";
    for (int i = 1; i <= num; i++)
    {
        scanf("%d", &temp);
        ListInsert(head, i, temp); // list, posi, element
    }

    cout << "before:" << endl;
    ListTest(head);

    // Revert List
    ListRevert(head);

    cout << "after:" << endl;
    ListTest(head);

    // release nodes
    DestroyList(head);

    return 0;
}