/*
 *2.38
 */
#include <iostream>
using namespace std;

struct LNode
{
    struct LNode *prior = NULL;
    int data;
    int freq = 0;
    struct LNode *next = NULL;
};

/**
 * @description: Create new node with certain element and insert to list
 * @param {*&LNode} L, {int} i, {double} e, {bool} isTail
 * @return {*}
 */
void ListInsert(LNode *&L, int i, double e, bool isTail) //Head, position, element
{
    LNode *cur = L;
    // new a space
    LNode *s = new LNode;
    s->data = e;
    // goto position
    for (int j = 1; j < i; j++)
        cur = cur->next;
    // get linked
    cur->next = s;
    s->prior = cur;
    // Tail process
    if (isTail)
    {
        s->next = L->next;
        L->next->prior = s;
    }
} // ListInsert

/**
 * @description: Output to test List
 * @param {*&LNode} L, 
 * @return {*}
 */
void ListTest(LNode *&L)
{
    LNode *cur = L->next;
    cout << endl;
    do
    {
        cout << cur->data << " ";
        cur = cur->next;
    } while (L->next != cur);
    cout << endl;
} // ListTest

/**
 * @description: shiftforward node 
 * @param {*&LNode} L, {*&LNode} Node 
 * @return {*}
 */
void ShiftForward(LNode *&L, LNode *&Node)
{
    LNode *selNode = Node;
    LNode *theNode = selNode;
    LNode *prior = theNode->prior;
    while (theNode->freq > theNode->prior->freq && L->next != theNode)
    {
        if (theNode->prior == L->next) //close to head
            L->next = theNode;

        theNode->next->prior = prior;
        prior->prior->next = theNode;

        theNode->prior = prior->prior;
        prior->next = theNode->next;

        prior->prior = prior->prior->next;
        prior->prior->next = prior;

        //point to origin Node
        theNode = selNode;
        prior = theNode->prior;
    }
} // ShiftForward

/**
 * @description: Locate nodes
 * @param {*&LNode} L, {const int} x
 * @return {int} location
 */
int Locate(LNode *&L, const int x)
{
    int location = 1;
    LNode *cur = L;
    while (x != cur->next->data)
    {
        cur = cur->next;
        location += 1;
        if (L != cur && L->next == cur->next) // loop
            return 0;
    }
    cur->next->freq += 1;
    ShiftForward(L, cur->next); // 遍历
    return location;
} //Locate

int main()
{
    freopen("../data3.txt", "r", stdin); // comment this line if you want to input data manually
    int num = 0, dest = 0, temp = 0;     // 个数，目标
    bool Continue = 0;
    LNode *head = new LNode; // 头结点

    cout << "Input number of data: ";
    cin >> num;

    // Create List
    cout << "Input data in order: ";
    for (int i = 1; i <= num; i++)
    {
        scanf("%d", &temp);
        ListInsert(head, i, temp, i == num); // list, posi, element
    }

    ListTest(head);
    do
    {
        cout << "input value x to find:" << endl;
        cin >> dest;
        cout << "Loaction:" << Locate(head, dest) << endl;
        ListTest(head);
        cout << "continue?(1/0): ";
        cin >> Continue;
    } while (Continue);

    return 0;
}