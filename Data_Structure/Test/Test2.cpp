#include <bits/stdc++.h>
using namespace std;
#define MAX_NODE_NUM 512
int n, m, u, v, q, x, y, k;
struct Graph
{
    int A[MAX_NODE_NUM][MAX_NODE_NUM];
    bool visited[MAX_NODE_NUM];
};
Graph g;
void Show(Graph &g, int n)
{
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= n; j++)
            cout << g.A[i][j] << " ";
        cout << endl;
    }
}
void Search(int head, int &len, bool &find)
{
    g.visited[head] = 1;
    for (int i = 1; i <= n; i++)
    {
        if (g.A[head][i] == 1 && g.visited[i] == false)
        {
            len += 1;
            if (len == k && i == y)
            {
                find = 1;
                return;
            }
            else if (len < k)
            {
                Search(i, len, find);
            }
            len -= 1;
        }
    }
    g.visited[head] = false;
}
void Judge(int x, int y, int k)
{
    // cout<<x<<" "<<y<<" "<<k<<endl;
    memset(g.visited, 0, sizeof(g.visited));
    if (k == 1 && g.A[x][y] != 1)
    {
        cout << "NO" << endl;
    }
    else
    {
        int len = 0;
        bool find;

        Search(x, len, find);
        
        if (!find)
            cout << "NO" << endl;
        else
            cout << "YES" << endl;
    }
}
int main()
{
    freopen("../data2.in", "r", stdin);
    freopen("../data2.out", "w", stdout);
    // int n,m,u,v,q,x,y,k;
    memset(g.A, 0, sizeof(g.A));
    memset(g.A, 0, sizeof(g.visited));
    cin >> n >> m;
    for (int i = 0; i < m; i++)
    {
        cin >> u >> v;
        g.A[u][v] = 1;
        g.A[v][u] = 1;
    }
    // Show(g,n);
    cin >> q;
    for (int i = 0; i < q; i++)
    {
        cin >> x >> y >> k;
        Judge(x, y, k);
    }
    return 0;
}