#include <bits/stdc++.h>
using namespace std;
/*先输入数，在链表中遍历对比，若无绝对值匹配再插入*/
struct node
{
    int val;
    node *next = NULL;
};
bool Transverse(node *head, int val)
{
    // cout<< val <<" "<< abs(val)<<endl;
    node *cur = head->next;
    while (cur != NULL)
    {
        if (abs(cur->val) == abs(val))
        {
            return 1;
        }
        cur = cur->next;
    }
    return 0;
}
void Insert(node *head, int val)
{
    node *cur = head;
    node *e = new node;
    e->val = val;
    while (cur->next != NULL)
    {
        cur = cur->next;
    }
    cur->next = e;
}
void Show(node *head)
{
    node *cur = head->next;
    while (cur != NULL)
    {
        cout << cur->val;
        if (cur->next == NULL)
            cout << " " << endl;
        else
            cout << " ";
        cur = cur->next;
    }
}
int main()
{
    freopen("../data1.in","r",stdin);
    // freopen("./data1.out","w",stdout);
    int n, val;
    node *head = new node;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> val;
        if (!Transverse(head, val)) // 无匹配
        {
            Insert(head, val);
        }
    }
    Show(head);
    return 0;
}