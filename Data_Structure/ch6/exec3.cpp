#include <bits/stdc++.h>
using namespace std;
// 按先序遍历的扩展序列建立二叉树的二叉链表存储结构，实现二叉树中序遍历的非递归算法，实现二叉树层次遍历。
// I:abd##e##c##
// O:dbeac abcde

struct BiTree
{
    char value;
    struct BiTree *Lchild = NULL;
    struct BiTree *Rchild = NULL;
    struct BiTree *parent = NULL;
};
stack<BiTree *> S;
queue<BiTree *> Q;

void Insert(BiTree *&T, BiTree *parent, int &i, string str, int &count)
{
    if (str[i] == '#')
        T = NULL;
    else
    {
        count++;
        T = new BiTree;
        T->value = str[i];
        T->parent = parent;
        Insert(T->Lchild, T, ++i, str, count);
        Insert(T->Rchild, T, ++i, str, count);
    }
}

void MidOrder(BiTree *&T)
{
    BiTree *cur = T;
    while (cur != NULL || !S.empty())
    {
        while (cur)
        {
            S.push(cur);
            cur = cur->Lchild;
        }
        if (!S.empty())
        {
            cur = S.top();
            cout << S.top()->value;
            S.pop();
            cur = cur->Rchild;
        }
    }
}

void LayerOrder(BiTree *&T)
{
    if (!T)
        return;
    BiTree *cur = T;
    Q.push(cur);
    while (!Q.empty())
    {
        if (Q.front()->Lchild)
            Q.push(Q.front()->Lchild);
        if (Q.front()->Rchild)
            Q.push(Q.front()->Rchild);
        cout << Q.front()->value;
        Q.pop();
    }
}

int main()
{
    freopen("../data3.in", "r", stdin);
    string input;
    int count = 0, numNode = 0;
    cin >> input;

    BiTree *root;
    BiTree *cur;
    Insert(root, NULL, count, input, numNode);

    cur = root;
    MidOrder(cur);
    cout << endl;

    cur = root;
    LayerOrder(cur);
    cout << endl;

    return 0;
}