#include <bits/stdc++.h>
using namespace std;
// 按先序遍历的扩展序列建立二叉树的二叉链表存储结构，实现二叉树先序、中序、后序遍历的递归算法，求二叉树的结点个数，求二叉树的深度。
// I: abd##e##c##
// O: abdec
// dbeac
// debca
// 5
// 3

struct BiTree
{
    char value;
    struct BiTree *Lchild = NULL;
    struct BiTree *Rchild = NULL;
};

void Insert(BiTree *&T, int &i, string str, int &count)
{
    if (str[i] == '#')
        T = NULL;
    else
    {
        count++;
        T = new BiTree;
        T->value = str[i];
        Insert(T->Lchild, ++i, str, count);
        Insert(T->Rchild, ++i, str, count);
    }
}
int Depth(BiTree *&T)
{
    if (T == NULL)
        return 0;
    int depth = 0, depthL = 0, depthR = 0;
    if (T->Lchild != NULL)
        depthL = Depth(T->Lchild);
    if (T->Rchild != NULL)
        depthR = Depth(T->Rchild);
    depth = (depthL > depthR ? depthL : depthR) + 1;
    return depth;
}

void PreOrder(BiTree *&T)
{
    if (T != NULL)
    {
        cout << T->value;
        PreOrder(T->Lchild);
        PreOrder(T->Rchild);
    }
}
void MidOrder(BiTree *&T)
{
    if (T != NULL)
    {
        MidOrder(T->Lchild);
        cout << T->value;
        MidOrder(T->Rchild);
    }
}
void PostOrder(BiTree *&T)
{
    if (T != NULL)
    {
        PostOrder(T->Lchild);
        PostOrder(T->Rchild);
        cout << T->value;
    }
}

int main()
{
    freopen("../data2.in", "r", stdin);
    string input;
    int count = 0, numNode = 0;
    cin >> input;

    BiTree *root;
    BiTree *cur;
    Insert(root, count, input, numNode);

    cur = root;
    PreOrder(cur);
    cout << endl;

    cur = root;
    MidOrder(cur);
    cout << endl;

    cur = root;
    PostOrder(cur);
    cout << endl;

    cout << numNode << endl;
    cout << Depth(root) << endl;

    return 0;
}