#ifndef _TREE_
#define _TREE_

class BiTree
{
public:
    int ID = 0;
    char value = 0;
    BiTree() {}
    BiTree(int eID)
    {
        ID = eID;
    }
    BiTree(char c)
    {
        value = c;
    }
    int depth()
    {
        int depth = 0, depthLeft = 0, depthRight = 0;
        if (this->Lchild == NULL && this->Rchild == NULL)
            depth = 0;
        else
        {
            if (this->Lchild != NULL)
                depthLeft = Lchild->depth();
            if (this->Rchild != NULL)
                depthRight = Rchild->depth();
            depth = 1 + (depthLeft > depthRight ? depthLeft : depthRight);
        }
        return depth;
    }
    void insert(char c, bool &full)
    {
        if (this->Lchild == NULL)
            this->Lchild = new BiTree(c);
        else if (this->Rchild == NULL)
        {
            this->Rchild = new BiTree(c);
            full = 1;
        }
    }
    class BiTree *parent = NULL;
    class BiTree *Lchild = NULL;
    class BiTree *Rchild = NULL;
};

#endif