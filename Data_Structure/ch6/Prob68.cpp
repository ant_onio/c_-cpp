#include <iostream>
#include <cstring>
using namespace std;
// #include "tree.h"

struct node
{
    int ID = 0;
    int lchild = 0;
    int rchild = 0;
    struct node *plchild = NULL;
    struct node *pnextsibling = NULL;
};

CSTree CreateCSTreeByLevelDegree(char *levelstr, int *num)
{
    int cnt, i, parent;
    CSNode *p;
    CSNode *tmp[maxSize];

    //先创建结点
    for (i = 0; i < strlen(levelstr); ++i)
    {
        p = (CSNode *)malloc(sizeof(CSNode));
        if (!p)
            exit(OVERFLOW);
        p->data = levelstr[i];
        p->firstchild = p->nextsibling = NULL;
        tmp[i] = p;
    }
    //连接
    parent = 0; //孩子的爸爸
    cnt = 0;    //计数器：表示已经找了几个孩子
    i = 1;      //遍历结点，为他们找爸爸
    while (i < strlen(levelstr))
    {
        if (num[parent] == 0 || cnt == num[parent])
        {             //这个父亲没有孩子 || parent的孩子已经找完了
            cnt = 0;  //计数器归0
            parent++; //位移一位
            continue;
        }
        //这个父亲有孩子（i是parent的孩子）
        cnt++;
        if (cnt == 1)
        { //i是parent的第一个孩子
            tmp[parent]->firstchild = tmp[i];
        }
        else
        {                                     //不是第一个孩子
            tmp[i - 1]->nextsibling = tmp[i]; //它是前面的兄弟
        }

        i++;
    }

    return tmp[0];
}

int main()
{
    freopen("../data68.in", "r", stdin);
    int x, degree;
    bool go(true);
    node *root = new node;

    while (go)
    {
        cin >> x >> degree;
    }

    cout << root->ID << endl;
    return 0;
}