#include <bits/stdc++.h>
using namespace std;
// 给定二叉树求其深度（注意：我们认为根结点的深度为1）
struct node
{
    int ID = 0;
    int lchild = 0;
    int rchild = 0;
    struct node *plchild = NULL;
    struct node *prchild = NULL;
};

int BiTreeDepth(node *&node)
{
    int depth = 0, depthLeft = 0, depthRight = 0;
    if (node->plchild == NULL && node->prchild == NULL)
        depth = 1;
    else
    {
        if (node->plchild != NULL)
            depthLeft = BiTreeDepth(node->plchild);
        if (node->prchild != NULL)
            depthRight = BiTreeDepth(node->prchild);
        depth = 1 + (depthLeft > depthRight ? depthLeft : depthRight);
    }
    return depth;
}

int main()
{
    freopen("../data1.in", "r", stdin);
    int n, r; // num, rootNum
    int x, y, z;
    cin >> n >> r;
    node *tree = new node[512];
    node *root = &tree[r];

    // BiTree *root = new BiTree(r);
    for (int i = 0; i < n; i++)
    {
        cin >> x >> y >> z;
        tree[x].ID = x;
        tree[x].lchild = y; // L
        tree[x].rchild = z; // R
    }

    for (int i = 1; i <= n; i++)
    {
        if (tree[i].lchild != 0)
            tree[i].plchild = &tree[tree[i].lchild];
        if (tree[i].rchild != 0)
            tree[i].prchild = &tree[tree[i].rchild];
    }
    cout << BiTreeDepth(root) << endl;

    return 0;
}