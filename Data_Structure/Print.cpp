// 1.打开setting->editor
// 2.在左边的选项栏中找到 Code completion
// 3.将Automatically launch when typed # letter中的4改成1，这样打1个字母就会有提示了。 
// 4.将Keyword sets to additionally include中1到9都勾上
// 5.将Delay for auto-kick-in when typing [.::->]拉到 200ms，这样瞬间就出来了
// 6.选中Case-sensitive match
// 7.找到Keyboard shortcuts
// 8.将Edit->Code complete的快捷键由Ctrl+Space改为Alt+/，这与eclipse的习惯是相同的，可以在提示不小心消失的情况下再次出现
// STL.size
// vector.push_back/pop_back/erase/begin/end/
// stack.push/pop
memset(void *dst,int val,void size);
memset(graph.A, false, sizeof(graph.A));

struct node //Tree
{
    int value;
    node *lchild;
    node *rchild;
    node *parent;
};
int BiTreeDepth(node *&node)
{ // 求二叉树深度（根为1）
    int depth = 0, depthLeft = 0, depthRight = 0;
    if (node->plchild == NULL && node->prchild == NULL)
        depth = 1;
    else
    {
        if (node->plchild != NULL)
            depthLeft = BiTreeDepth(node->plchild);
        if (node->prchild != NULL)
            depthRight = BiTreeDepth(node->prchild);
        depth = 1 + (depthLeft > depthRight ? depthLeft : depthRight);
    }
    return depth;
}
void PreOrder(BiTree *&T)
{ // 先根序
    if (T != NULL)
    {
        cout << T->value;
        PreOrder(T->Lchild);
        PreOrder(T->Rchild);
    }
}
void MidOrder(BiTree *&T)
{ // 中根序-递归
    if (T != NULL)
    {
        MidOrder(T->Lchild);
        cout << T->value;
        MidOrder(T->Rchild);
    }
}
void MidOrder(BiTree *&T)
{ // 中根序-非递归
    stack<BiTree *> S;
    BiTree *cur = T;
    while (cur != NULL || !S.empty())
    {
        while (cur)
        {
            S.push(cur);
            cur = cur->Lchild;
        }
        if (!S.empty())
        {
            cur = S.top();
            cout << S.top()->value;
            S.pop();
            cur = cur->Rchild;
        }
    }
}
void PostOrder(BiTree *&T)
{ // 后根序
    if (T != NULL)
    {
        PostOrder(T->Lchild);
        PostOrder(T->Rchild);
        cout << T->value;
    }
}
void LayerOrder(BiTree *&T)
{ // 层序遍历
    queue<BiTree *> Q;
    if (!T)
        return;
    BiTree *cur = T;
    Q.push(cur);
    while (!Q.empty())
    {
        if (Q.front()->Lchild)
            Q.push(Q.front()->Lchild);
        if (Q.front()->Rchild)
            Q.push(Q.front()->Rchild);
        cout << Q.front()->value;
        Q.pop();
    }
}

//Graph 邻接表
struct VNode
{ // 头
    bool visited = false;
    vector<int> arc; // arc
};
struct ALGraph  //Graph
{               // 图
    int vexnum; // num of node
    int arcnum; // num of arc
    int count = 0;
    queue<int> Qbfs;
    VNode vertices[MAX_NODE]; // head node[]
};
void DFS(ALGraph &graph, int w)
{
    cout << w;
    if (graph.count < graph.vexnum)
        cout << " ";
    for (uint32_t i = 0; i < graph.vertices[w].arc.size(); i++)
        if (!graph.vertices[graph.vertices[w].arc[i]].visited)
        {
            graph.vertices[graph.vertices[w].arc[i]].visited = true;
            graph.count += 1;
            DFS(graph, graph.vertices[w].arc[i]);
        }
}
void DFSTraverse(ALGraph graph, int w)
{
    // reset
    graph.count = 0;
    for (int i = 1; i <= graph.vexnum; i++)
        graph.vertices[i].visited = false;
    // transverse
    graph.vertices[w].visited = true;
    graph.count += 1;
    DFS(graph, w);
    cout << endl;
}
void BFSTraverse(ALGraph graph, int w)
{
    graph.count = 0;
    graph.Qbfs.push(w);
    graph.vertices[w].visited = true;

    while (graph.count < graph.vexnum)
    {
        for (uint32_t i = 0; i < graph.vertices[graph.Qbfs.front()].arc.size(); i++)
        {
            if (!graph.vertices[graph.vertices[graph.Qbfs.front()].arc[i]].visited)
            {
                graph.Qbfs.push(graph.vertices[graph.Qbfs.front()].arc[i]);
                graph.vertices[graph.vertices[graph.Qbfs.front()].arc[i]].visited = true;
            }
        }
        cout << graph.Qbfs.front();
        graph.Qbfs.pop();
        graph.count += 1;
        if (graph.count < graph.vexnum)
            cout << " ";
        else
            cout << endl;
    }
}
void ArcSort(ALGraph &graph)
{ // 对弧排序
    int temp;
    for (int i = 1; i <= graph.vexnum; i++)
        for (uint32_t j = 0; j < graph.vertices[i].arc.size() - 1; j++)
            for (uint32_t k = j + 1; k < graph.vertices[i].arc.size(); k++)
                if (graph.vertices[i].arc[j] > graph.vertices[i].arc[k])
                {
                    temp = graph.vertices[i].arc[j];
                    graph.vertices[i].arc[j] = graph.vertices[i].arc[k];
                    graph.vertices[i].arc[k] = temp;
                }
}
// Graph-邻接矩阵
struct AMGraph //Graph
{              // 图
    int vexnum;
    int arcnum;
    int count = 0;
    queue<int> bfs;
    bool A[MAX_NODE][MAX_NODE];
    bool visited[MAX_NODE];
};
void DFS(AMGraph &graph, int w)
{
    cout << w;
    if (graph.count < graph.vexnum)
        cout << " ";
    for (int i = 1; i <= graph.vexnum; i++)
        if (graph.A[w][i] && !graph.visited[i])
        {
            graph.visited[i] = true;
            graph.count += 1;
            DFS(graph, i);
        }
}
void DFSTraverse(AMGraph graph, int w)
{
    // reset
    graph.count = 0;
    for (int i = 1; i <= graph.vexnum; i++)
        graph.visited[i] = false;
    // transverse
    graph.visited[w] = true;
    graph.count += 1;
    DFS(graph, w);
    cout << endl;
}

void BFSTraverse(AMGraph graph, int w)
{
    graph.count = 0;
    graph.bfs.push(w);
    graph.visited[w] = true;

    while (graph.count < graph.vexnum)
    {
        for (int i = 1; i <= graph.vexnum; i++)
            if (graph.A[graph.bfs.front()][i] && !graph.visited[i])
            {
                graph.bfs.push(i);
                graph.visited[i] = true;
            }
        cout << graph.bfs.front();
        graph.bfs.pop();
        graph.count += 1;
        if (graph.count < graph.vexnum)
            cout << " ";
        else
            cout << endl;
    }
}