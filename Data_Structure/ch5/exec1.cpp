#include <iostream>
using namespace std;

struct ele
{
    int row = -1;
    int col = -1;
    int value;
    struct ele *next = NULL;
};
void Insert(ele *&head, int row, int col, int value)
{
    ele *e = new ele;
    e->row = row;
    e->col = col;
    e->value = value;
    ele *cur = head;
    while (cur->next != NULL)
        cur = cur->next;
    cur->next = e;
}
void transposition(ele *&head)
{
    ele *cur = head->next;
    int temp = 0;
    while (cur != NULL)
    {
        temp = cur->row;
        cur->row = cur->col;
        cur->col = temp;
        cur = cur->next;
    }
}
void show(ele *&head)
{
    ele *cur = head->next;
    while (cur != NULL)
    {
        cout << cur->row << " " << cur->col << " " << cur->value << endl;
        cur = cur->next;
    }
}

int main()
{
    // freopen("../exec1.txt", "r", stdin);
    int numRow, numCow;
    int r, c, v;
    int go = 1;
    cin >> numRow >> numCow;
    ele *head = new ele;
    do
    {
        cin >> r >> c >> v;
        Insert(head, r, c, v);
        cout << "continue?(0/1)" << endl;
        cin >> go;
    } while (go);
    cout<<"Origin:"<<endl;
    show(head);
    transposition(head);
    cout<<"Trans:"<<endl;
    show(head);
    return 0;
}