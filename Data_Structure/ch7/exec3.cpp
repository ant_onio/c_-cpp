#include <bits/stdc++.h>
#define MAX_NODE 512
using namespace std;
// 建立无向图G的邻接矩阵或邻接表存储结构（任选一种），使用prim算法求图G的最小生成树，输出各条边及最小生成树的代价。

struct AMGraph //Graph
{              // 图
    int vexnum;
    int arcnum;
    int count = 0;
    queue<int> bfs;
    int A[MAX_NODE][MAX_NODE];
    bool visited[MAX_NODE];
};

struct CloseEdge
{
    int adjVex = 0;
    int lowCost = 501;
};

void test(AMGraph &graph)
{
    //*/ test
    for (int i = 1; i <= graph.vexnum; i++)
    {
        for (int j = 1; j <= graph.vexnum; j++)
            cout << graph.A[i][j] << " ";
        cout << endl;
    } //*/
    cout << endl;
}

void Prim(AMGraph &graph, int r)
{
    // init
    graph.A[0][0] = 501;
    CloseEdge CE[MAX_NODE];
    int cost = 0;
    int minDes = 0;
    graph.count = 0;
    for (int i = 1; i <= graph.vexnum; i++)
        for (int j = 1; j <= graph.vexnum; j++)
            if (graph.A[i][j] == 0)
                graph.A[i][j] = 501;
    for (int i = 1; i <= graph.vexnum; i++)
        graph.visited[i] = false;

    // gen---------------------------------------------------------------
    graph.visited[r] = true;
    CE[r].adjVex = -1;
    for (int i = 1; i < graph.vexnum; i++)
    {
        minDes = 0;
        for (int j = 1; j <= graph.vexnum; j++)
        {
            if (!graph.visited[j] && graph.A[r][j] != 501) // unTransversed
                if (CE[j].lowCost > graph.A[r][j] || CE[j].adjVex == -1)
                {
                    CE[j].adjVex = r;
                    CE[j].lowCost = graph.A[r][j];
                }
        }

        /*/ test
        cout << "adjVex  ";
        for (int j = 1; j <= graph.vexnum; j++)
            cout << CE[j].adjVex << " ";
        cout << endl;
        cout << "lowCost ";
        for (int j = 1; j <= graph.vexnum; j++)
            cout << CE[j].lowCost << " ";
        cout << endl;
        //*/

        for (int j = 1; j <= graph.vexnum; j++)
            // if (CE[j].adjVex == r && CE[j].lowCost < CE[minDes].lowCost)
            if (CE[j].lowCost < 501 && CE[j].lowCost < CE[minDes].lowCost)
            {
                r = CE[j].adjVex;
                minDes = j;
            }

        cout << r << " " << minDes << " " << CE[minDes].lowCost << endl;
        cost += CE[minDes].lowCost;
        r = minDes;
        CE[r].adjVex = -1;
        CE[r].lowCost = 501;
        graph.visited[r] = true;
    }
    cout << cost << endl;
}

int main()
{
    freopen("../data1.in", "r", stdin);
    // freopen("../data2.out", "w", stdout);
    int n, m, u, v, w, r;
    AMGraph graph;
    memset(graph.A, 0, sizeof(graph.A));
    memset(graph.visited, false, sizeof(graph.visited));

    cin >> n >> m;
    graph.vexnum = n;
    graph.arcnum = m;
    for (int i = 0; i < graph.arcnum; i++)
    {
        cin >> u >> v >> w;
        if (graph.A[u][v] != 0 && graph.A[v][u] != 0)
            i -= 1;
        else
        {
            graph.A[u][v] = w;
            graph.A[v][u] = w;
        }
    }
    cin >> r;

    // test(graph);

    Prim(graph, r);

    return 0;
}