#include <bits/stdc++.h>
#define MAX_NODE 512
using namespace std;
// 建立无向图G的邻接表存储结构，求其从任意顶点出发的广度优先搜索序列与深度优先搜索序列。（每个点优先去往周围未到达的点中编号最小的点）

struct VNode
{ // 头
    bool visited = false;
    vector<int> arc; // arc
};
struct ALGraph  //Graph
{               // 图
    int vexnum; // num of node
    int arcnum; // num of arc
    int count = 0;
    queue<int> Qbfs;
    VNode vertices[MAX_NODE]; // head node[]
};

void DFS(ALGraph &graph, int w)
{
    cout << w;
    if (graph.count < graph.vexnum)
        cout << " ";
    for (uint32_t i = 0; i < graph.vertices[w].arc.size(); i++)
        if (!graph.vertices[graph.vertices[w].arc[i]].visited)
        {
            graph.vertices[graph.vertices[w].arc[i]].visited = true;
            graph.count += 1;
            DFS(graph, graph.vertices[w].arc[i]);
        }
}
void DFSTraverse(ALGraph graph, int w)
{
    // reset
    graph.count = 0;
    for (int i = 1; i <= graph.vexnum; i++)
        graph.vertices[i].visited = false;
    // transverse
    graph.vertices[w].visited = true;
    graph.count += 1;
    DFS(graph, w);
    cout << endl;
}
void BFSTraverse(ALGraph graph, int w)
{
    graph.count = 0;
    graph.Qbfs.push(w);
    graph.vertices[w].visited = true;

    while (graph.count < graph.vexnum)
    {
        for (uint32_t i = 0; i < graph.vertices[graph.Qbfs.front()].arc.size(); i++)
        {
            if (!graph.vertices[graph.vertices[graph.Qbfs.front()].arc[i]].visited)
            {
                graph.Qbfs.push(graph.vertices[graph.Qbfs.front()].arc[i]);
                graph.vertices[graph.vertices[graph.Qbfs.front()].arc[i]].visited = true;
            }
        }
        cout << graph.Qbfs.front();
        graph.Qbfs.pop();
        graph.count += 1;
        if (graph.count < graph.vexnum)
            cout << " ";
        else
            cout << endl;
    }
}

void ArcSort(ALGraph &graph)
{
    int temp;
    for (int i = 1; i <= graph.vexnum; i++)
        for (uint32_t j = 0; j < graph.vertices[i].arc.size() - 1; j++)
            for (uint32_t k = j + 1; k < graph.vertices[i].arc.size(); k++)
                if (graph.vertices[i].arc[j] > graph.vertices[i].arc[k])
                {
                    temp = graph.vertices[i].arc[j];
                    graph.vertices[i].arc[j] = graph.vertices[i].arc[k];
                    graph.vertices[i].arc[k] = temp;
                }
}

//*/ test
void Test(ALGraph graph)
{
    for (int i = 1; i <= graph.vexnum; i++)
    {
        for (uint32_t j = 0; j < graph.vertices[i].arc.size(); j++)
            cout << graph.vertices[i].arc[j] << " ";
        cout << endl;
    }
    cout << endl;
} //*/

int main()
{
    // init
    freopen("../data1.in", "r", stdin);
    // freopen("../data2.out", "w", stdout);
    int n, m, u, v, w;
    bool repeat = false;
    ALGraph graph;

    cin >> n >> m;
    graph.vexnum = n;
    graph.arcnum = m;
    for (int i = 0; i < graph.arcnum; i++)
    {
        cin >> u >> v;
        for (uint32_t j = 0; j < graph.vertices[u].arc.size(); j++)
            if (graph.vertices[u].arc[j] == v)
            {
                repeat = true;
                break;
            }
        if (!repeat)
            for (uint32_t j = 0; j < graph.vertices[v].arc.size(); j++)
                if (graph.vertices[v].arc[j] == v)
                {
                    repeat = true;
                    break;
                }
        if (repeat)
            i -= 1;
        else
        {
            graph.vertices[u].arc.push_back(v);
            graph.vertices[v].arc.push_back(u);
        }
        repeat = false;
    }
    cin >> w; // w 头节点

    Test(graph);
    ArcSort(graph);
    Test(graph);

    // process
    if (w > graph.vexnum)
        cout << "起始顶点不存在" << endl;
    else
    {
        BFSTraverse(graph, w);
        DFSTraverse(graph, w);
    }
    return 0;
}