#include <bits/stdc++.h>
using namespace std;

#define MAX_NODE 512

struct VNode
{ // 头
    bool visited = false;
    vector<int> arc;    // arc
    vector<int> weight; // weight
};
struct ALGraph  //Graph
{               // 图
    int vexnum; // num of node
    int arcnum; // num of arc
    int count = 0;
    queue<int> Qbfs;
    VNode vertices[MAX_NODE]; // head node[]
};

void ArcSort(ALGraph &graph)
{
    int temp;
    for (int i = 1; i <= graph.vexnum; i++)
        for (uint32_t j = 0; j < graph.vertices[i].arc.size() - 1; j++)
            for (uint32_t k = j + 1; k < graph.vertices[i].arc.size(); k++)
                if (graph.vertices[i].arc[j] > graph.vertices[i].arc[k])
                {
                    temp = graph.vertices[i].arc[j];
                    graph.vertices[i].arc[j] = graph.vertices[i].arc[k];
                    graph.vertices[i].arc[k] = temp;
                }
}

//*/ test
void Test(ALGraph graph)
{
    for (int i = 1; i <= graph.vexnum; i++)
    {
        for (uint32_t j = 0; j < graph.vertices[i].arc.size(); j++)
            cout << graph.vertices[i].arc[j] << " ";
        cout << endl;
    }
    cout << endl;
} //*/

void Input(ALGraph &graph)
{
    int n, m, u, v;
    bool repeat = false;
    cin >> n >> m;
    graph.vexnum = n;
    graph.arcnum = m;
    for (int i = 0; i < graph.arcnum; i++)
    {
        cin >> u >> v;
        for (uint32_t j = 0; j < graph.vertices[u].arc.size(); j++)
            if (graph.vertices[u].arc[j] == v)
            {
                repeat = true;
                break;
            }
        if (!repeat)
            for (uint32_t j = 0; j < graph.vertices[v].arc.size(); j++)
                if (graph.vertices[v].arc[j] == v)
                {
                    repeat = true;
                    break;
                }
        if (repeat)
            i -= 1;
        else
        {
            graph.vertices[u].arc.push_back(v);
            graph.vertices[v].arc.push_back(u);
        }
        repeat = false;
    }
}

bool BFS(ALGraph &graph, int vi, int vj)
{
    graph.count = 0;
    while (graph.count < graph.vexnum)
    {
        for (uint32_t i = 0; i < graph.vertices[graph.Qbfs.front()].arc.size(); i++)      //每条边
        {                                                                                 //
            if (!graph.vertices[graph.vertices[graph.Qbfs.front()].arc[i]].visited)       //判断已遍历
            {                                                                             //
                graph.Qbfs.push(graph.vertices[graph.Qbfs.front()].arc[i]);               //入队
                graph.vertices[graph.vertices[graph.Qbfs.front()].arc[i]].visited = true; //标记已遍历
            }
        }
        if (graph.Qbfs.front() == vj) //判断符合
            return true;
        else
        {
            graph.Qbfs.pop(); //出队
            graph.count += 1; //计数
        }
    }
    return false;
}

int PathLen(ALGraph &graph, int vi, int vj, int k)
{

    //判断邻接表方式存储的有向图G的顶点i到j是否存在长度为k的简单路径
    if (vi == vj && k == 0)
        return 1; //找到了一条路径,且长度符合要求
    else if (k > 0)
    {
        graph.vertices[vi].visited = 1;
        for (p = graph.vertices[vi].firstarc; p != NULL; p = p->nextarc)
        {
            m = p->adjvex;
            if (!graph.vertices[m].visited)
                if (PathLen(graph, m, vj, k - 1))
                    return 1; //剩余路径长度减一
        }                     //for
        graph.vertices[vi].visited[vi] = 0;       //本题允许曾经被访问过的结点出现在另一条路径中
                              //这里需要把已经访问的点重新置为0，因为如果当前不存在长度为k
                              //到达j点，那么这个点还是可以使用的，因为有可能从其他点出发
                              //可以到达j点并且长度为k
                              //可以看下图解释
    }                         //else
    return 0;                 //没找到
}

bool DFS(ALGraph &graph, int vi, int vj)
{
    graph.count += 1;                                               //遍历计数
    for (int i = 0; i < graph.vertices[vi].arc.size(); i++)         //该结点所有子树
        if (!graph.vertices[graph.vertices[vi].arc[i]].visited)     //i号子树未遍历
            if (graph.vertices[vi].arc[i] != vj)                    //i号子树不符合
                return (DFS(graph, graph.vertices[vi].arc[i], vj)); //递归
            else                                                    //符合
                return true;                                        //返回
    if (graph.count >= graph.vexnum)                                //无路径
        return false;                                               //返回
}

bool DFSSearch(ALGraph &graph, int vi, int vj)
{
    graph.vertices[vi].visited = true;
    for (uint32_t i = 0; i < graph.vertices[vi].arc.size(); i++)
        if (!graph.vertices[graph.vertices[vi].arc[i]].visited) // 未遍历
        {
            if (graph.vertices[vi].arc[i] == vj)
                return true;
            else if (!graph.vertices[vi].visited)
                DFSSearch(graph, graph.vertices[vi].arc[i], vj);
        }
}

int main()
{
    // init
    int vi, vj;
    freopen("../data1.in", "r", stdin);
    // freopen("../data1.out", "w", stdout);
    ALGraph graph;
    Input(graph);
    cin >> vi >> vj; // w 头节点
    ArcSort(graph);
    Test(graph);

    // Process
    cout << (DFSSearch(graph, vi, vj) ? "True" : "False") << endl;

    return 0;
}