#include <bits/stdc++.h>
#define MAX_NODE 512
using namespace std;
// 建立无向图G的邻接矩阵存储结构，求其从任意顶点出发的广度优先搜索序列与深度优先搜索序列。（每个点优先去往周围未到达的点中编号最小的点）

struct AMGraph //Graph
{              // 图
    int vexnum;
    int arcnum;
    int count = 0;
    queue<int> bfs;
    bool A[MAX_NODE][MAX_NODE];
    bool visited[MAX_NODE];
};

void DFS(AMGraph &graph, int w)
{
    cout << w;
    if (graph.count < graph.vexnum)
        cout << " ";
    for (int i = 1; i <= graph.vexnum; i++)
        if (graph.A[w][i] && !graph.visited[i])
        {
            graph.visited[i] = true;
            graph.count += 1;
            DFS(graph, i);
        }
}
void DFSTraverse(AMGraph graph, int w)
{
    // reset
    graph.count = 0;
    for (int i = 1; i <= graph.vexnum; i++)
        graph.visited[i] = false;
    // transverse
    graph.visited[w] = true;
    graph.count += 1;
    DFS(graph, w);
    cout << endl;
}

void BFSTraverse(AMGraph graph, int w)
{
    graph.count = 0;
    graph.bfs.push(w);
    graph.visited[w] = true;

    while (graph.count < graph.vexnum)
    {
        for (int i = 1; i <= graph.vexnum; i++)
            if (graph.A[graph.bfs.front()][i] && !graph.visited[i])
            {
                graph.bfs.push(i);
                graph.visited[i] = true;
            }
        cout << graph.bfs.front();
        graph.bfs.pop();
        graph.count += 1;
        if (graph.count < graph.vexnum)
            cout << " ";
        else
            cout << endl;
    }
}

int main()
{
    freopen("../data1.in", "r", stdin);
    // freopen("../data2.out", "w", stdout);
    int n, m, u, v, w;
    AMGraph graph;
    memset(graph.A, false, sizeof(graph.A));
    memset(graph.visited, false, sizeof(graph.visited));

    cin >> n >> m;
    graph.vexnum = n;
    graph.arcnum = m;
    for (int i = 0; i < graph.arcnum; i++)
    {
        cin >> u >> v;
        if (graph.A[u][v] == true && graph.A[v][u] == true)
            i -= 1;
        else
        {
            graph.A[u][v] = true;
            graph.A[v][u] = true;
        }
    }
    cin >> w;

    /*/ test
    for (int i = 1; i <= graph.vexnum; i++)
    {
        for (int j = 1; j <= graph.vexnum; j++)
            if (graph.A[i][j])
                cout << "1 ";
            else
                cout << "0 ";
        cout << endl;
    } //*/

    // process
    if (w > graph.vexnum)
        cout << "起始顶点不存在" << endl;
    else
    {
        BFSTraverse(graph, w);
        DFSTraverse(graph, w);
    }

    return 0;
}