#include <bits/stdc++.h>
using namespace std;
// 构造一棵二叉排序树并对其进行中序遍历输出。
// 在二叉排序树中查找某一关键字，若存在，显示“查找成功”以及查找成功时关键字比较次数;若不存在，将其插入到二叉排序树中，再中序遍历输出。

struct Tnode
{
    int val = 1000;
    int depthL = 0;
    int depthR = 0;
    Tnode *L = NULL;
    Tnode *R = NULL;
};

void Insert(Tnode *&node, int x)
{
    if (x < node->val)
    {
        if (node->L == NULL)
        {
            Tnode *e = new Tnode;
            e->val = x;
            node->L = e;
        }
        else
            Insert(node->L, x);
    }
    else if (x > node->val)
    {
        if (node->R == NULL)
        {
            Tnode *e = new Tnode;
            e->val = x;
            node->R = e;
        }
        else
            Insert(node->R, x);
    }
}

void Show(Tnode *&node, int &count, int n)
{
    count += 1;
    if (node->L != NULL)
        Show(node->L, count, n);
    cout << node->val;
    if (count < n)
        cout << " ";
    if (node->R != NULL)
        Show(node->R, count, n);
}

int Search(Tnode *&node, int x)
{
    if (x == node->val)
        return 1;
    else if (x < node->val && node->L != NULL)
        return Search(node->L, x) + 1;
    else if (x > node->val && node->R != NULL)
        return Search(node->R, x) + 1;
    else
        return -32768;
}

int main()
{
    // freopen("../data1.in", "r", stdin);
    // freopen("../data1.out", "w", stdout);

    Tnode *root = new Tnode;
    int n = 0, x = 0;
    int count = 0;
    int temp = 0;
    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> temp;
        Insert(root, temp);
    }
    cin >> x;

    Show(root->L, count, n);
    cout << endl;

    count = Search(root->L, x);
    if (count > 0)
        cout << "查找成功 " << count << endl;
    else
    {
        Insert(root, x);
        cout << "插入成功" << endl;
        count = 0;
        Show(root->L, count, n + 1);
        cout << endl;
    }

    return 0;
}