#include <bits/stdc++.h>
using namespace std;

bool cmp(int a, int b)
{
    return a < b;
}
int main()
{
    // freopen("../data1.in", "r", stdin);
    // freopen("../data1.out", "w", stdout);

    int n = 0;
    int x = 0;
    int count = 0;
    int temp = 0;
    bool ins = 1;
    vector<int> a;
    a.push_back(-1);

    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> temp;
        a.push_back(temp);
    }
    cin >> x;
    sort(a.begin() + 1, a.end(), cmp);
    for (int i = 1; i < n; i++)
        cout << a[i] << " ";
    cout << a[n] << endl;

    int low = 1;
    int high = n;
    int cur;

    while (cur != 0)
    {
        count += 1;
        cur = (low + high) / 2;
        if (x != a[cur] && low == high)
            break;
        if (x == a[cur])
        {
            cout << "查找成功 " << count << endl;
            ins = 0;
            break;
        }
        else if (x < a[cur])
            high = cur - 1;
        else if (x > a[cur])
            low = cur + 1;
    }
    if (ins)
    {
        a.push_back(x);
        sort(a.begin() + 1, a.end(), cmp);
        cout << "插入成功" << endl;

        for (long long unsigned int i = 1; i < a.size()-1; i++)
            cout << a[i] << " ";
        cout << a[n + 1] << endl;
    }

    return 0;
}