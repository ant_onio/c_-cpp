#include <bits/stdc++.h>
using namespace std;
#define H_L 15
// 已知哈希表长为15，哈希函数为H(key)=key mod 11，线性探测再散列解决冲突，输入n个关键字，建立哈希表（地址为0…14，初始均为-999）；任意输入关键字，判断是否在哈希表中，若存在显示查找成功并显示查找次数；若不存在，插入到哈希表中，再显示哈希表中的关键字序列。

int Hash(int key)
{
    return key % 11;
}
void Insert(vector<int> &H, int e)
{
    int addr = Hash(e);
    while (1)
    {
        if (H[addr] == -999)
        {
            H[addr] = e;
            break;
        }
        if (addr == 14)
            addr = 0;
        else
            addr += 1;
    }
}

void Search(vector<int> &H, int e)
{
    int addr = Hash(e);
    bool found = 0;
    int count = 0;
    while (1)
    {
        count += 1;
        if (e == H[addr])
        {
            found = true;
            cout << "查找成功 " << count << endl;
            break;
        }
        else if (addr == H_L - 1)
            addr = 0;
        else if (addr == Hash(e) && count > 1)
            break;
        else
            addr += 1;
    }
    if (!found)
    {
        addr = Hash(e);
        while (1)
        {
            if (H[addr] == -999)
            {
                H[addr] = e;
                cout << "插入成功 " << addr << endl;
                break;
            }
            if (addr == 14)
                addr = 0;
            else
                addr += 1;
        }
    }
}

int main()
{
    freopen("../data1.in", "r", stdin);
    freopen("../data1.out", "w", stdout);
    vector<int> hashTable(H_L);
    int n = 0, x = 0;
    int temp;
    for (int i = 0; i < H_L; i++)
        hashTable[i] = -999;

    cin >> n;
    for (int i = 0; i < n; i++)
    {
        cin >> temp;
        Insert(hashTable, temp);
    }
    cin >> x;

    for (int i = 0; i < H_L - 1; i++)
        cout << hashTable[i] << " ";
    cout << hashTable[H_L - 1] << endl;

    // Search
    Search(hashTable, x);

    return 0;
}