#include <bits/stdc++.h>
#include <stack>
using namespace std;

template <class T>
class Array
{
protected:
    int size;
    T *elements;

public:
    Array(int i);
    virtual ~Array();
    Array(const Array &a);
    T operator[](int index);
    friend ostream &operator<<(ostream &o, Array &a);
};

template <typename T>
inline T const &Max(T const &a, T const &b) { return a < b ? b : a; }

int main()
{

    return 0;
}
