#include<cmath>
#include<iostream>
using namespace std;

int visit[9], arr[9], n = 9;
bool is_sqrt(int num)
{
    return pow(int(sqrt(num)),2) == num; 
}//返回条件判定

void dfs(int count)
{
    if(count == n)//1 判定是否到达dfs下线
    {
        int fir = arr[0] * 100 + arr[1] * 10 + arr[2];
        int sec = arr[3] * 100 + arr[4] * 10 + arr[5];
        int thi = arr[6] * 100 + arr[7] * 10 + arr[8];
        if(is_sqrt(fir)&&is_sqrt(sec)&&is_sqrt(thi))// 1.1 判定是否满足输出条件
        {
            for(int j = 0; j < int(sqrt(n)); j++)
            {
                for(int i = 0; i < int(sqrt(n)); i++)
                    cout << arr[i+3*j] ;
                cout << endl;
            }
            cout << endl;
        }
        return;
    }

    for(int i = 0; i < n; i++)//2遍历
    {
        if(visit[i])//dfs入栈判定
            continue;
        visit[i] = 1;//入栈
        arr[count] = i + 1;//入栈，需要注意题目是从1到9而不是从0到8
        dfs(count + 1);//入栈+出栈
        visit[i] = 0;//出栈
    }
}
int main()
{
    dfs(0);
    return 0;
}

