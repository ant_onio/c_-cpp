class preson;
class company;
class Program;

class person
{
public:
    string name;

    person(string eName)
    {
        name = eName;
    }

    void startWork()
    {
        Console.WriteLine("员工{0}开始工作", name);
    }
};

class company
{
public:
    string name; //公司名称
    person[] employee = new person[3];

    company(string cName) //构造函数
    {
        name = cName;
    }

    void run()
    {
        Console.WriteLine("公司“{0}”开始运作", name);
        employee[0].startWork();
        employee[1].startWork();
        employee[2].startWork();
    }

    void stop()
    {
        Console.WriteLine("公司“{0}”停止运作", name);
    }
};
class Program
{
    static void Main(string[] args)
    {
        company c = new company("北京能力有限公司");
        c.employee[0] = new person("张三");
        c.employee[1] = new person("李四");
        c.employee[2] = new person("王二");
        c.run();
        c.stop();
        Console.Read();
    }
}