#include<iostream>
using namespace std;
//*/
int count_exp=0;    //爆破数量和
int if_exp[200000]={0};     //爆破状态
int cur_city=0;     //当前城市(0开始)
int bribery=0;      //贿赂次数
int cost=0;         //花费和
int last_act=0;     //上一次动作：0后退，1前进
int skip_valid=1;   //默认可通过传送进行转移
int count_done=0;   //程序跑完
int n[200005]={0},x[200005]={0},c[200005][2005]={0};
//*/

int main()
{
    // freopen("./test.in","r",stdin);
    // freopen("./test.out","w",stdout);
    // init
    int T;  //数据组数
    scanf("%d",&T);
    // int n[100005],x[100005],c[100005][2005];
    
    /*/
    int count_exp=0;    //爆破数量和
    int if_exp[100005]={0};  //爆破状态
    int cur_city=0;     //当前城市(0开始)
    int bribery=0;      //贿赂次数
    int cost=0;         //花费和
    int last_act=0;     //上一次动作：0后退，1前进
    int skip_valid=1;   //默认可通过传送进行转移
    int count_done=0;   //程序跑完//*/
    for(int i=0;i<T;i++)    //input-T组数据
    {
        scanf("%d%d",&n[i],&x[i]);
        for(int j=0;j<n[i];j++){scanf("%d",&c[i][j]);}
    }
    /*/test
    cout<<T<<endl;
    for(int i=0;i<T;i++)
    {
        cout<<n[i]<<" "<<x[i]<<endl;
        for(int j=0;j<n[i];j++){cout<<c[i][j]<<" ";}
        cout<<endl;
    }//*/
    //loop T
    for(int i=0;i<T;i++)
    {   
        // cout<<"loop:"<<i<<endl;
        //init values----------------------------------------------------------
        for(int j=0;j<n[i];j++){if_exp[j]=0;}    //复位爆破判断
        bribery=0,cost=0;                        //复位贿赂次数、花费
        count_exp=0,cur_city=0;
        last_act=0;
        skip_valid=1;
        //run(初始于城市0)------------------------------------------------------
        if(n[i]%x[i]==0 && x[i]!=1){skip_valid=0;}//能整除，传送非单步，无法使用传送
        //*/优先全传送，完成-------------------------------------------------------------
        while(skip_valid)   //可传送
        {
            if_exp[cur_city]=1,count_exp++;
            if(cur_city+x[i]>=n[i])              //再向前传送会溢出
            {cur_city=cur_city+x[i]-n[i];}      //更新当前城市(前进)
            else
            {cur_city+=x[i];}

            if(count_exp<n[i] && cur_city==0) //没炸完 又到了开头
            {skip_valid=0;} //一直跳不可行
            if(count_exp==n[i])     //能炸完，跳出循环
            {break;}
        }//*/
        if(!skip_valid)  //一直传送不可行
        {
            // init
            for(int j=0;j<n[i];j++){if_exp[j]=0;}    //复位爆破判断()
            bribery=0,cost=0;   //复位贿赂次数、花费
            count_exp=0,cur_city=0;
            last_act=0;
            /*/
            while(count_exp<n[i]) //没炸完
            {
                // cout<<"position:"<<cur_city<<endl;
                if(last_act==0)//上次后退了(初始步)(前进步)(需判断上次退回来的是否已经被炸)
                {
                    if(if_exp[cur_city]==1)   //退回来的城市已经被炸(需前移)
                    {
                        bribery++,cost+=c[i][cur_city];   //更新贿赂次数，花费qwq
                        cur_city++;                         //向前挪一位
                    }
                    else if(if_exp[cur_city]==0)   //退回来的城市没被炸(可前传送)
                    {
                        if_exp[cur_city]=1,count_exp++;     //更新爆破状态
                        if(cur_city+x[i]>n[i])             //再向前传送会溢出
                        {cur_city=cur_city+x[i]-n[i];}      //更新当前城市(前进)
                        else
                        {
                            cur_city+=x[i];
                            last_act=1;                     //更新动作状态
                        }
                    }
                }
                else if(last_act==1) //上次前进了(后退步)
                {
                    if_exp[cur_city]=1,count_exp++;     //更新爆破状态
                    if(cur_city-x[i]<0)                 //再后退传送会溢出
                    {cur_city=cur_city-x[i]+n[i];}      //更新当前城市(后退)
                    else
                    {
                        cur_city-=x[i];                 //直接后传送
                        last_act=0;
                    }                   
                }
            }//*/
        }
        //output---------------------------------------------------------------
        printf("%d\n",bribery*cost);
    }

    return 0;
}