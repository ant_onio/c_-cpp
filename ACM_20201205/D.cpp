#include<iostream>
#include<cstdio>
using namespace std;
int main()
{
    // freopen("./test.in","r",stdin);
    // freopen("./test.out","w",stdout);
    //init
    long long int N,M,H,h;  //连招长度，敌人的数量, 喵喵的HP, 当前血量
    scanf("%lld%lld%lld",&N,&M,&H);
    long long int R[N+3]={0},E[N+5]={0};      //R、E生效秒数值
    long int att00=0,att01=0,att10=0,att11=0; //该招数序列对应对方技能 能攻击次数和
    int A[M],B[M],C[M]; //一个敌人的特性
    /*
    A-1=只要能够施展轻功，即使仍然被控制也可以攻击
    B-1=永远不会陷入被控制状态
    C-攻击力
    */
    char str[N];            //喵喵的连招
    scanf("%s",str);
    for(int i=0;i<N;i++)    //input&mark_skill (N*3or5)
    {
        if(str[i]=='R')   //release R-anti_light
        {for(int k=0;k<3;k++){R[i+k]=1;}}
        else if(str[i]=='E')  //release E_ctrl
        {for(int k=0;k<5;k++){E[i+k]=1;}}
    }
    for(int i=0;i<M;i++){scanf("%d%d%d",&A[i],&B[i],&C[i]);} //(M)
    /*/test
    cout<<N<<" "<<M<<" "<<H<<endl;
    cout<<"str:";
    for(int i=0;i<N;i++){cout<<(char)str[i]<<" ";}cout<<endl;
    // for(int i=0;i<M;i++){cout<<A[i]<<" "<<B[i]<<" "<<C[i]<<endl;}//*/
    //PreProcess
    /*/test_marking
    cout<<"R:  ";
    for(int i=0;i<N;i++){cout<<R[i]<<" ";}cout<<endl;
    cout<<"E:  ";
    for(int i=0;i<N;i++){cout<<E[i]<<" ";}cout<<endl<<endl;//*/

    //mark attack
    for(int i=4;i<N;i++)    //(N-4)
    {
        if(R[i]==0)
        {
            //                   check             check 
            if(E[i]==0){att00+=1,att01+=1,att10+=1,att11+=1;}
            else       {att00+=0,att01+=1,att10+=1,att11+=1;}
        }
        else if(R[i]==1)
        {
            if(E[i]==0){att00+=(0+1),att01+=1,att10+=(0+1),att11+=1;}
            else       {att00+=0,att01+=1,att10+=0,att11+=1;}
        }
    }
    /*/
    cout<<"att00:"<<att00<<endl;
    cout<<"att01:"<<att01<<endl;
    cout<<"att10:"<<att10<<endl;
    cout<<"att11:"<<att11<<endl<<endl;//*/
    //count
    for(int i=0;i<M;i++)    //M个敌人循环M次(M)
    {
        h=H;    //reset health复位血量
        if(A[i]==0)
        {
            if(B[i]==0){h-=att00*C[i];}
            else{h-=att01*C[i];}
        }
        else if(A[i]==1)
        {
            if(B[i]==0){h-=att10*C[i];}
            else{h-=att11*C[i];}
        }
        /*/
        for(int j=4;j<N;j++)    //回合次数，共N秒
        {
            //if(j>=4)
            {   
                //      轻功       不受控      抑制轻功    控制
                if     (A[i]==1 && B[i]==0 && R[j]==0 && E[j]==0){att[j]=1;}
                else if(A[i]==1 && B[i]==0 && R[j]==0 && E[j]==1){att[j]=1;}
                else if(A[i]==1 && B[i]==0 && R[j]==1 && E[j]==0){att[j]=0;}
                else if(A[i]==1 && B[i]==0 && R[j]==1 && E[j]==1){att[j]=0;}

                else if(A[i]==1 && B[i]==1 && R[j]==0 && E[j]==0){att[j]=1;}
                else if(A[i]==1 && B[i]==1 && R[j]==0 && E[j]==1){att[j]=1;}
                else if(A[i]==1 && B[i]==1 && R[j]==1 && E[j]==0){att[j]=1;}
                else if(A[i]==1 && B[i]==1 && R[j]==1 && E[j]==1){att[j]=1;}

                else if(A[i]==0 && B[i]==1 && R[j]==0 && E[j]==0){att[j]=1;}
                else if(A[i]==0 && B[i]==1 && R[j]==0 && E[j]==1){att[j]=1;}
                else if(A[i]==0 && B[i]==1 && R[j]==1 && E[j]==0){att[j]=1;}
                else if(A[i]==0 && B[i]==1 && R[j]==1 && E[j]==1){att[j]=1;}

                else if(A[i]==0 && B[i]==0 && R[j]==0 && E[j]==0){att[j]=1;}
                else if(A[i]==0 && B[i]==0 && R[j]==0 && E[j]==1){att[j]=0;}
                else if(A[i]==0 && B[i]==0 && R[j]==1 && E[j]==0){att[j]=0;}
                else if(A[i]==0 && B[i]==0 && R[j]==1 && E[j]==1){att[j]=0;}
            }
            h-=att[j]*C[i];
        }//*/

        if(h>0)
        {printf("WIN %d\n",h);}
        else //if(h<=0)
        {printf("LOSE %d\n",h);}
    }
    return 0;
}