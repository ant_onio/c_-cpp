//存放调用实验4程序生成数据记录文件的函数
#include "Lab6_call.h"
#include "Lab6_data.h"

extern CONF conf; //参数信息
extern FILE *fp;

void callFile(char filetype)
{
    char buffer[8][MAX_STR_LEN];
    char *ext;
    // 打开文件,读文件名
    fp = fopen("./conf.ini", "r");
    for (int i = 0; i < 8; i++)
    {
        fgets(buffer[i], MAX_STR_LEN, fp);
    }
    fclose(fp);

    // 修改扩展名,存conf
    ext = strrchr(buffer[1], '.') + 1;
    *ext = '\0';
    switch (filetype)
    {
    case 'T':
        strcat(buffer[1], "txt");
        break;
    case 'B':
        strcat(buffer[1], "dat");
        break;
    }
    memset(conf.filename, 0, sizeof(conf.filename));
    strcat(conf.filename, buffer[1]);
    strcat(buffer[1], "\n");

    // 打开文件，写回修改
    fp = fopen("./conf.ini", "w+");
    for (int i = 0; i < 8; i++) //写入所有处理后的数据
    {
        fputs(buffer[i], fp);
    }
    fclose(fp);

    //调用Lab4
    char *CMD;
    // CMD = (char *)malloc(20);
    CMD = new char;
    strcpy(CMD, "");
    strcat(CMD, "./Lab4 ");
    if (conf.number == -1)
    {
        strcat(CMD, "r");
    }
    else
    {
        char num[8];                     //8位数
        sprintf(num, "%d", conf.number); //(int)number转换存到(char)num
        strcat(CMD, num);
    }
    system(CMD);
    free(CMD);
}
