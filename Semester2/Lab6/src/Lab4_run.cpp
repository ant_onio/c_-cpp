//存储run函数的函数实现
#include <iostream>
#include <cstdio>
#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <ctime>
#include "Lab4_run.h"
#include "Lab4_fun.h"
#include "Lab6_data.h"

CONF config = {0};

void Argv1(char *argv, CONF *conf);
void Argv3(char *argv, CONF *conf);
void GenFile(CONF *Data, int type);
int ReadConfig(CONF *conf);

/*
*函数名称：Run
*函数功能：
*输入参数：int argc：命令行参数个数, char* argv[]:命令行参数值
*返回值：无
*版本信息：create by 董逸箫，2021-04-08
*/
void Run(int argc, char *argv[])
{
    /*/for test
    cout << "argc:" << argc << endl;
    for (int i = 0; i < argc; i++)
    {
        cout << "argv[" << i << "]:" << argv[i] << endl;
    } //*/

    //Init
    // srand(rand() + (unsigned)time(0)); //以时间+随机数为种子

    //声明结构体变量
    CONF conf = {0};
    // DATAITEM Cache = {0};

    //读取默认配置文件
    // if(ReadConfig(&conf) == NULL) //read failed
    // {return;} //?
    ReadConfig(&conf);
    //*/ check config read
    {
        printf("Config:\n");
        printf("path:%s\nname:%s\n", conf.filesavepath, conf.filename);
        printf("number:%d\n", conf.number);
        printf("value1: %d,%d\n", conf.maxvalue1, conf.minvalue1);
        printf("value2: %d,%d\n", conf.maxvalue2, conf.minvalue2);
        printf("number: %d,%d\n", conf.recordcount1, conf.recordcount2);
    }
    //*/

    //*/ --------------- argc==2 ----------------------------------
    //文件名Lab3; 格式-按conf.ini
    if (argc == 2) //num or 'r'
    {
        // cout<<"1参数"<<endl;
        if (!isNumber(argv[1])) //合法性检查，参数1不合法时
        {
            puts("argv1 invalid!");
            return;
        }
        else if (isNumber(argv[1]) == 1) // 数字
        {
            if (conf.recordcount1 < atoi(argv[1]))      //大于上限
                conf.number = conf.recordcount1;        //设为上限
            else if (conf.recordcount2 > atoi(argv[1])) //小于下限
                conf.number = conf.recordcount2;        //设为下限
            else
                conf.number = atoi(argv[1]);
        }
        else if (isNumber(argv[1]) == 2) //随机
            conf.number = GetRand(conf.recordcount1, conf.recordcount2);

        //生成文件
        if (strstr(conf.filename, ".txt") != NULL)
            GenFile(&conf, 1);
        else if (strstr(conf.filename, ".dat") != NULL)
            GenFile(&conf, 2);
    } //*/
    else
    {
        cout << "Invalid Parameter!" << endl;
        // return;
    }
}