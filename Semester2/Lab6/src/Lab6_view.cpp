/* notify 显示后即释放 */
#include "Lab6_main.h"
#include "Lab6_call.h"
#include "Lab6_loadfile.h"
#include "Lab6_view.h"
#include "Lab6_conf.h"
#include "Lab6_data.h"

int **tempCont; //读入临时容器变量
int record_num;

void Main_menu()
{
    int opt = -1;
    // Display
    do
    {
        system("clear");
        puts("--------------------------------------------------------------------------------------------------");
        puts("|  董逸箫的 Lab6 程序                                                                            |");
        puts("--------------------------------------------------------------------------------------------------");
        puts("|  主菜单                                                                                        |");
        puts("--------------------------------------------------------------------------------------------------");
        puts("|  1-----调用实验 4 程序生成记录文件（文本方式）                                                 |");
        puts("|  2-----调用实验 4 程序生成记录文件（二进制方式）                                               |");
        puts("|  3-----读取指定数据记录文件（二维数组存储方式）                                                |");
        puts("|  4-----读取指定数据记录文件（结构体数组存储方式）                                              |");
        puts("|  5-----读取指定数据记录文件（指针数组存储方式）                                                |");
        puts("|  6-----读取指定数据记录文件（链表存储方式）                                                    |");
        puts("|  7-----调用实验 4 生成数据记录文件，同时读取数据记录文件（文本方式输出，二维数组方式存储）     |");
        puts("|  8-----调用实验 4 生成数据记录文件，同时读取数据记录文件（文本方式输出，结构体数组方式存储）   |");
        puts("|  9-----调用实验 4 生成数据记录文件，同时读取数据记录文件（文本方式输出，指针数组方式存储）     |");
        puts("|  10----调用实验 4 生成数据记录文件，同时读取数据记录文件（文本方式输出，链表方式存储）         |");
        puts("|  11----调用实验 4 生成数据记录文件，同时读取数据记录文件（二进制方式输出，二维数组方式存储）   |");
        puts("|  12----调用实验 4 生成数据记录文件，同时读取数据记录文件（二进制方式输出，结构体数组方式存储） |");
        puts("|  13----调用实验 4 生成数据记录文件，同时读取数据记录文件（二进制方式输出，指针数组方式存储）   |");
        puts("|  14----调用实验 4 生成数据记录文件，同时读取数据记录文件（二进制方式输出，链表方式存储）       |");
        puts("|  15----重新设置配置参数值                                                                      |");
        // puts("|  16----Test                                                                                    |");
        puts("|  0-----退出                                                                                    |");
        puts("--------------------------------------------------------------------------------------------------");

        L1_menu_input(&opt);
    } while (opt != 0);
}

void setPara_menu()
{
    int opt = -1;
    // Display
    do
    {
        system("clear");
        printf("--------------------------------------------------------------------------------------------------\n");
        puts("|  董逸箫的 Lab6 程序                                                                            |");
        puts("--------------------------------------------------------------------------------------------------");
        puts("|  配置参数值                            | 当前值                                                |");
        puts("--------------------------------------------------------------------------------------------------");
        printf("|  1-----修改文件存储位置                | %s\n", conf.filesavepath);
        printf("|  2-----修改文件名                      | %s\n", conf.filename);
        printf("|  3-----修改数据记录条数                | %d\n", conf.number);
        printf("|  4-----修改第一、二个元素最大值        | %d\n", conf.maxvalue1);
        printf("|  5-----修改第一、二个元素最小值        | %d\n", conf.minvalue1);
        printf("|  6-----修改第三个元素最大值            | %d\n", conf.maxvalue2);
        printf("|  7-----修改第三个元素最小值            | %d\n", conf.minvalue2);
        printf("|  8-----修改输入记录条数最大值          | %d\n", conf.recordcount1);
        printf("|  9-----修改输入记录条数最小值          | %d\n", conf.recordcount2);
        printf("|  10----修改程序工作模式                | %d (%s)\n", conf.runMode, !conf.runMode ? "Auto" : "Interactive");
        puts("|  0-----返回                                                                                    |");
        puts("--------------------------------------------------------------------------------------------------");

        L2_menu_input(&opt);
    } while (opt != 0);

    strcpy(notify, "配置参数已保存");
}

void L1_menu_input(int *opt)
{
    printf("%s\n\n", notify);
    if (*notify != '\0')
        // free(notify);
        delete notify;
    printf("请输入您要执行的程序序号：");
    // Input
    scanf("%d", opt);
    getchar(); //吃掉回车

    switch (*opt)
    {
    case 0:
        printf("Exit Program\n");
        return;
    case 1:
        callFile('T');
        strcpy(notify, "已生成文本记录文件");
        break;
    case 2:
        callFile('B');
        strcpy(notify, "已生成二进制记录文件");
        break;
    case 3:
        ReadFile(tempCont);  //读文件
        save2Darr(tempCont); //存数组
        strcpy(notify, "二维数组方式读取文件");
        break;
    case 4:
        ReadFile(tempCont);
        saveStructArr(tempCont);
        strcpy(notify, "结构体数组方式读取文件");
        break;
    case 5:
        ReadFile(tempCont);
        savePointerArr(tempCont);
        strcpy(notify, "指针数组方式读取文件");
        break;
    case 6:
        ReadFile(tempCont);
        saveLLarr(tempCont);
        strcpy(notify, "链表方式读取文件");
        break;
    case 7:
        callFile('T');
        ReadFile(tempCont);  //读文件
        save2Darr(tempCont); //存数组
        strcpy(notify, "");
        break;
    case 8:
        callFile('T');
        ReadFile(tempCont);
        saveStructArr(tempCont);
        strcpy(notify, "");
        break;
    case 9:
        callFile('T');
        ReadFile(tempCont);
        savePointerArr(tempCont);
        strcpy(notify, "");
        break;
    case 10:
        callFile('T');
        ReadFile(tempCont);
        saveLLarr(tempCont);
        strcpy(notify, "");
        break;
    case 11:
        callFile('B');
        ReadFile(tempCont);  //读文件
        save2Darr(tempCont); //存数组
        strcpy(notify, "");
        break;
    case 12:
        callFile('B');
        ReadFile(tempCont);
        saveStructArr(tempCont);
        strcpy(notify, "");
        break;
    case 13:
        callFile('B');
        ReadFile(tempCont);
        savePointerArr(tempCont);
        strcpy(notify, "");
        break;
    case 14:
        callFile('B');
        ReadFile(tempCont);
        saveLLarr(tempCont);
        strcpy(notify, "");
        break;
    case 15:
        setPara_menu();
        break;
    /* case 16:
        system("./build/Lab4");
        break; */
    default:
        strcpy(notify, "输入序号错误！请重新输入。");
        *opt = -1;
        return;
    }
}

void L2_menu_input(int *opt)
{
    printf("%s\n\n", notify);

    printf("请输入您要配置的参数序号：");
    // Input
    scanf("%d", opt);
    getchar(); //吃掉回车

    switch (*opt)
    {
    case 0:
        printf("conf Saved, Exit Menu\n");
        saveConf();
        return;
    case 1:
        printf("请输入文件路径:");
        // getFilesavepath();
        getstring(conf.filesavepath, "//");
        FormatCONF(); // 规范化路径
        strcpy(notify, "文件路径已保存");
        break;
    case 2:
        printf("请输入文件名:");
        getFilename();
        // getchar(conf.filename, "/");
        strcpy(notify, "文件名已保存");
        break;
    case 3:
        printf("请输入生成的数据条数(r为随机):");
        getvalue(&conf.number, 1);
        strcpy(notify, "数据组数已保存");
        break;
    case 4:
        printf("请输入第一、二元素的最大值:");
        getvalue(&conf.maxvalue1, 0);
        strcpy(notify, "一、二元素最大值已保存");
        break;
    case 5:
        printf("请输入第一、二元素的最小值:");
        getvalue(&conf.minvalue1, 0);
        strcpy(notify, "一、二元素最小值已保存");
        break;
    case 6:
        printf("请输入第三元素的最大值:");
        getvalue(&conf.maxvalue2, 0);
        strcpy(notify, "三元素最大值已保存");
        break;
    case 7:
        printf("请输入第三元素的最小值:");
        getvalue(&conf.minvalue2, 0);
        strcpy(notify, "三元素最小值已保存");
        break;
    case 8:
        printf("请输入记录条数上限:");
        getvalue(&conf.recordcount1, 0);
        strcpy(notify, "记录条数最大值已保存");
        break;
    case 9:
        printf("请输入记录条数下限:");
        getvalue(&conf.recordcount2, 0);
        strcpy(notify, "记录条数最小值已保存");
        break;
    case 10:
        // system("clear");
        printf("指定程序工作模式：\n自动模式: 0\n交互模式: 1\n请输入: ");
        scanf("%d", &conf.runMode);
        if (conf.runMode == 0)
        {
            char buffer[MAX_STR_LEN];
            fp = popen("pwd", "r");
            fscanf(fp, "%s\n", buffer);
            pclose(fp);
            strcpy(conf.filesavepath, buffer);
            strcat(conf.filesavepath, "/DataSet");

            strcpy(conf.filename, "DataFile.txt");
        }

        fp = fopen("./modeCONF.ini", "w+");
        fprintf(fp, "%d\n", conf.runMode);
        fclose(fp);

        strcpy(notify, "模式已设置");
        break;
    default:
        strcpy(notify, "输入序号错误！请重新输入。");
        *opt = -1;
        return;
    }
}