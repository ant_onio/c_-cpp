#include "Lab6_call.h"
#include "Lab6_loadfile.h"
#include "Lab6_data.h"

/**
 * @description: Read data File to 2D array (tempCont)
 * @param {** & int} cont
 * @return {*}
 */
void ReadFile(int **&cont)
{
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    // judge filetype
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        if ((fp = fopen(path, "r")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fscanf(fp, "%d", &record_num); //得行数
        cont = (int **)malloc(sizeof(int *) * (record_num + 1));
        // cont = new int *; //行
        for (int i = 0; i < record_num; i++)
        {
            // tempCont[i] = (int *)malloc(sizeof(int) * 3); //3列
            cont[i] = new int;
            fscanf(fp, "%d,%d,%d\n", &cont[i][0], &cont[i][1], &cont[i][2]);
        }
    }
    else
    { //DAT
        if ((fp = fopen(path, "rb")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fread(&record_num, 4, 1, fp); //
        cont = (int **)malloc(sizeof(int *) * (record_num + 1));
        // cont = new int *; //行
        for (int i = 0; i < record_num; i++)
        {
            // tempCont[i] = (int *)malloc(sizeof(int) * 3); //3列
            cont[i] = new int;
            fread(cont[i], 4, 3, fp);
        }
    }

    /*/test
    for (int i = 0; i < record_num; i++)
        printf("%d: %d,%d,%d\n", i, cont[i][0], cont[i][1], cont[i][2]);
    //*/
    fclose(fp);
}
/**
 * @description: 倒序读入cont数据,以便排序,存回
 * @param {**&int} oriCont 源二维数组
 * @return {*}
 */
void save2Darr(int **&oriCont)
{
    int sorCont[record_num][3]; //排序后容器变量
    for (int i = 0; i < record_num; i++)
    {
        for (int j = 0; j < 3; j++)
            sorCont[i][j] = oriCont[i][j];
    }
    qsort(sorCont, record_num, sizeof(sorCont[0]), cmp2D);

    //*/ test
    printf("|     | 排序前\t| 排序后\t|\n");
    for (int i = 0; i < record_num; i++)
        printf("| %3d | %3d,%3d,%3d | %3d,%3d,%3d |\n", i, oriCont[i][0], oriCont[i][1], oriCont[i][2], sorCont[i][0], sorCont[i][1], sorCont[i][2]);
    getchar();
    //*/
}
void saveStructArr(int **&oriCont)
{
    DataItem sorCont[record_num];
    for (int i = 0; i < record_num; i++)
    {
        sorCont[i].item1 = oriCont[i][0];
        sorCont[i].item2 = oriCont[i][1];
        sorCont[i].item3 = oriCont[i][2];
    }
    qsort(sorCont, record_num, sizeof(sorCont[0]), cmpStruct);
    //*/ test
    printf("|     | 排序前\t| 排序后\t|\n");
    for (int i = 0; i < record_num; i++)
        printf("| %3d | %3d,%3d,%3d | %3d,%3d,%3d |\n", i, oriCont[i][0], oriCont[i][1], oriCont[i][2], sorCont[i].item1, sorCont[i].item2, sorCont[i].item3);
    getchar();
    //*/
}
void savePointerArr(int **&oriCont)
{
    int *sorCont[record_num];
    for (int i = 0; i < record_num; i++)
    {
        sorCont[i] = new int;
        for (int j = 0; j < 3; j++)
            sorCont[i][j] = oriCont[i][j];
    }
    // sort
    qsort(sorCont, record_num, sizeof(sorCont[0]), cmpPointer);
    //*/ test
    printf("|     | 排序前\t\t| 排序后\t|\n");
    for (int i = 0; i < record_num; i++)
        printf("| %3d | %3d,%3d,%3d | %3d,%3d,%3d |\n", i, oriCont[i][0], oriCont[i][1], oriCont[i][2], sorCont[i][0], sorCont[i][1], sorCont[i][2]);
    for (int i = 0; i < 3; i++)
        delete sorCont[i];
    getchar();
    //*/
}
void saveLLarr(int **&oriCont)
{
    Container *Node;
    Node = (Container *)malloc(sizeof(Container) * (record_num + 1));
    Container *Head = &Node[0];
    Container *current;
    // Container *next;
    Container *temp1;
    // Container *temp2;
    // 仅使用存前后地址
    temp1 = new Container;
    // temp2 = new Container;
    // next = new Container;
    // save data
    for (int i = 0; i < record_num; i++)
    {
        // 测试！
        // Node[i].item[0] = i + 2;
        for (int j = 0; j < 3; j++)
            Node[i].item[j] = oriCont[i][j];

        Node[i].pNext = &Node[i + 1];
        // &(Node[i + 1].pNext)->pLast = Node[i]; // 双向链表
    }
    for (int i = record_num - 1; i >= 0; i--)
    {
        Node[i].pPrevious = &Node[i - 1];
    }
    current = Head; // From Head Head->pPrevious= NULL;
    // sort
    /*/ 交换结点方法，弃用
    for (int i = 0; i < record_num; i++)
    {
        current = Head; // From Head Head->pPrevious= NULL;
        for (int j = 0; (j < record_num - 2 - i || j == 0); j++)
        {

            next->pPrevious = (current->pNext)->pPrevious; //指向current
            next->pNext = (current->pNext)->pNext;         //指向current的下下个

            // compare item3 and sort
            if (current == Head) // current 地址
            {
                if (current->item[2] > (current->pNext)->item[2])
                {
                    Head = current->pNext;
                    (current->pNext)->pNext = next->pPrevious;
                    current->pNext = next->pNext;
                    current = Head; // 保持current位置不变（还是Head）
                    Head->pPrevious = NULL;
                }
            }

            // next->pPrevious = &current; //指向current
            next->pNext = (current->pNext)->pNext; //指向current的下下个

            if (i == 7 && j == 41)
                getchar();
            if ((current->pNext)->item[2] > (next->pNext)->item[2])
            {
                if ((next->pNext)->pNext != NULL)
                    (current->pNext)->pNext = (next->pNext)->pNext;
                (next->pNext)->pNext = current->pNext;
                current->pNext = next->pNext;
            }
            printf("i:%d, j:%d, last:%d,%d\n", i, j, ((current->pNext)->pNext)->item[0], ((current->pNext)->pNext)->item[2]);
            current = current->pNext;
            // printf("%d,%d,%d,%d\n\n", Head->item[2], (Head->pNext)->item[2], ((Head->pNext)->pNext)->item[2], (((Head->pNext)->pNext)->pNext)->item[2]);
        }
    } //*/
    //交换数据域方法
    for (int i = 0; i < record_num; i++)
    {
        current = Head; // From Head Head->pPrevious= NULL;
        for (int j = 0; (j < record_num - 1 - i || j == 0); j++)
        {
            if (current->item[2] > (current->pNext)->item[2])
            {
                for (int i = 0; i < 3; i++)
                    temp1->item[i] = (current->pNext)->item[i];
                for (int i = 0; i < 3; i++)
                    (current->pNext)->item[i] = current->item[i];
                for (int i = 0; i < 3; i++)
                    current->item[i] = temp1->item[i];
            }
            current = current->pNext;
        }
    }
    // display
    printf("%d\n", record_num);
    current = Head;
    printf("| %3d | 排序前\t\t| 排序后\t|\n", record_num);
    for (int i = 0; i < record_num; i++)
    {
        printf("| %3d | %3d,%3d,%3d | %3d,%3d,%3d |\n", i, oriCont[i][0], oriCont[i][1], oriCont[i][2], current->item[0], current->item[1], current->item[2]);
        current = current->pNext; //Point to next
    }
    delete Node;
    delete temp1;
    // delete temp2;
    // delete next;

    getchar();
}
/**
 * @description: 
 * @param {const void *} a  
 * @param {const void *} b
 * @return {?}
 */
int cmp2D(const void *a, const void *b)
{
    int *pa = (int *)a;
    int *pb = (int *)b;
    return (*(pa + 2) - *(pb + 2));
}
int cmpStruct(const void *a, const void *b)
{
    DATAITEM *pa = (DATAITEM *)a;
    DATAITEM *pb = (DATAITEM *)b;
    return ((*pa).item3 - (*pb).item3);
}
int cmpPointer(const void *a, const void *b)
{
    DATAITEM **pa = (DATAITEM **)a;
    DATAITEM **pb = (DATAITEM **)b;
    return ((**pa).item3 - (**pb).item3);
}