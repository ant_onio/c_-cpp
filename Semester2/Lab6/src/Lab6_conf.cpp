//存放与修改配置文件相关的功能函数
#include "Lab6_conf.h"
#include "Lab6_data.h"

extern CONF conf;
extern FILE *fp;

/*
读路径并统一为绝对路径存入conf.ini
若不存在则创建
*/
void FormatCONF()
{
    // init
    char *buffer;
    char *CMD;
    CMD = (char *)malloc(MAX_STR_LEN); //要用malloc，太长
    // CMD = new char;
    buffer = (char *)malloc(MAX_STR_LEN);
    // buffer = new char;

    /*/ check path
    strcpy(CMD, "find ");
    strcat(CMD, conf.filesavepath);
    fp = popen(CMD, "r");
    fscanf(fp, "%s\n", buffer); // !
    // fgets(buffer, MAX_STR_LEN, fp);
    pclose(fp);
    //*/

    // if (strspn(buffer, conf.filesavepath) != strlen(buffer)) // 路径不存在-mkdir
    // {
    strcpy(CMD, "mkdir ");
    strcat(CMD, conf.filesavepath);
    fp = popen(CMD, "r");
    pclose(fp);
    // }

    //get abs path and save
    strcpy(CMD, "cd ");
    strcat(CMD, conf.filesavepath);
    strcat(CMD, "\npwd");
    fp = popen(CMD, "r");
    fscanf(fp, "%s\n", buffer);
    std::cout << buffer << std::endl;
    strcpy(conf.filesavepath, buffer);
    pclose(fp);

    // 更新conf.ini
    char Buffer[8][MAX_STR_LEN];
    // fp = NULL;
    fp = fopen("./conf.ini", "a+");
    for (int i = 0; i < 8; i++) //读
    {
        fgets(Buffer[i], MAX_STR_LEN, fp);
        // fscanf(fp, "%s", Buffer[i]);
    }
    fclose(fp);
    // 修改路径
    strcpy(Buffer[0], "");
    strcat(Buffer[0], conf.filesavepath);
    strcat(Buffer[0], "\n");
    // 打开文件，写回修改
    fp = fopen("./conf.ini", "w+");
    for (int i = 0; i < 8; i++) //写入所有处理后的数据
    {
        fputs(Buffer[i], fp);
    }
    printf("Filepath.conf:%s\n", Buffer[0]);
    fclose(fp);

    //post process
    // delete[] CMD;
    free(CMD);
    // delete buffer;
    free(buffer);
    fp = NULL;
}
// 获取数值
void getvalue(int *destination, bool randomAvailable)
{
    char *num;
    num = new char;
    num[0] = 'a'; //init with err code
    if (randomAvailable)
    {
        while (!(strspn(num, "r") == strlen(num) || strspn(num, "0123456789") == strlen(num)))
        {
            scanf("%s", num);
            if (strspn(num, "r") == strlen(num)) //有且仅有r
                *destination = -1;
            else if (strspn(num, "0123456789") == strlen(num)) //纯数字
                *destination = atoi(num);
            else
                printf("错误输入，重新输入：");
        }
    }
    else if (!randomAvailable)
    {
        while (!(strspn(num, "0123456789") == strlen(num)))
        {
            scanf("%s", num);
            if (strspn(num, "0123456789") == strlen(num)) //纯数字
                *destination = atoi(num);
            else
                printf("错误输入，重新输入：");
        }
    }
    delete num;
    // 修改任何一项数值 更改为交互模式
    conf.runMode = 1;
}
// 获取字符串
void getstring(char *destination, std::string exclude)
{
    scanf("%s", conf.filesavepath);
    // 检查合法性
    // Validate();

    // 修改任何一项数值 更改为交互模式
    conf.runMode = 1;
}
void getFilename()
{
    scanf("%s", conf.filename);
    // 检查合法性
    // Validate();

    // 修改任何一项数值 更改为交互模式
    conf.runMode = 1;
}
void saveConf()
{
    //写入buffer
    char Buffer[8][MAX_STR_LEN];
    sprintf(Buffer[0], "%s\n", conf.filesavepath);
    sprintf(Buffer[1], "%s\n", conf.filename);
    sprintf(Buffer[2], "%d\n", conf.maxvalue1);
    sprintf(Buffer[3], "%d\n", conf.minvalue1);
    sprintf(Buffer[4], "%d\n", conf.maxvalue2);
    sprintf(Buffer[5], "%d\n", conf.minvalue2);
    sprintf(Buffer[6], "%d\n", conf.recordcount1);
    sprintf(Buffer[7], "%d\n", conf.recordcount2);

    //更新conf.ini
    // 打开文件，写回修改
    fp = fopen("./conf.ini", "w+");
    for (int i = 0; i < 8; i++) //写入所有处理后的数据
    {
        fputs(Buffer[i], fp);
    }
    fclose(fp);
}
