# Lab6 
## 文件结构
```
.
├── CMakeLists.txt
├── Lab4.cpp
├── Lab6.cpp
├── conf.ini
├── include
│   ├── Lab4_fun.h
│   ├── Lab4_run.h
│   ├── Lab6_call.h
│   ├── Lab6_conf.h
│   ├── Lab6_data.h
│   ├── Lab6_loadfile.h
│   ├── Lab6_main.h
│   └── Lab6_view.h
├── modeCONF.ini
├── readme.md
├── src
│   ├── Lab4_fun.cpp
│   ├── Lab4_run.cpp
│   ├── Lab6_call.cpp
│   ├── Lab6_conf.cpp
│   ├── Lab6_loadfile.cpp
│   ├── Lab6_main.cpp
│   └── Lab6_view.cpp
└── test.cpp
```
## 编译
1. 环境 Ubuntu 20.04.02 LTS, GCC 9.3.0, cmake 3.16.3  
2. 进入目录 `cd ./Lab6/build`  
   编译 `build ..`
3. 运行 `./Lab6`

# Issue 
## #1 完善getstring函数中检查合法性功能
## #2 精简函数
## #3 Lab4 中增加判断一二元素相等