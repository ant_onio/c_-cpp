//存放修改配置文件相关功能函数的函数声明
#ifndef LAB6_CONF_H
#define LAB6_CONF_H

#include <iostream>
#include <string.h>
#include <string>
#include <malloc.h>

extern char *notify; //提示信息

//交互模式
void FormatCONF();
void getvalue(int *destination, bool randomAvailable);
void getstring(char *destination, std::string exclude);
// void getFilesavepath();
void getFilename();
void saveConf();

#endif