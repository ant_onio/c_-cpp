#ifndef __LAB6_LOADFILE_H__
#define __LAB6_LOADFILE_H__

extern FILE *fp;
extern int record_num;
// new

void ReadFile(int **&cont);
void save2Darr(int **&oriCont);
void saveStructArr(int **&oriCont);
void savePointerArr(int **&oriCont);
void saveLLarr(int **&oriCont);
int cmp2D(const void *a, const void *b);
int cmpStruct(const void *a, const void *b);
int cmpPointer(const void *a, const void *b);

#endif