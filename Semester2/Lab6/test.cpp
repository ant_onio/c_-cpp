#include <iostream>
#include <vector>
#include <string.h>
#include <malloc.h>
#include "Lab6_data.h"

using namespace std;

void LL()
{
    struct Container Node1;
    Node1.pPrevious = NULL;
    Node1.item[0] = 1;
    Node1.item[1] = 2;
    Node1.item[2] = 3;
    Node1.pNext = NULL;

    getchar();
}

void shell()
{
    FILE *fp;
    char *buffer;
    char *CMD;
    buffer = new char;
    CMD = new char;
    strcpy(CMD, "ls ");
    fp = popen(CMD, "r");
    fscanf(fp, "%s\n", buffer);
    // cout << buffer << endl;
    printf("%s", buffer);

    pclose(fp);
}
int cmp(const void *a, const void *b)
{
    int *c = (int *)a;
    int *d = (int *)b;
    if (*c != *d)
        return *c - *d;
    return *(d + 1) - *(c + 1);
}
void sort2Darr()
{
    int a[4][3] = {
        {2, 5, 4},
        {4, 7, 8},
        {3, 8, 5},
        {6, 9, 1}};
    cout << "before:" << endl;
    for (int i = 0; i < 4; i++)
        cout << a[i][0] << "," << a[i][1] << "," << a[i][2] << "," << endl;

    qsort(a, 4, sizeof(a[0]), cmp);

    cout << "after:" << endl;
    for (int i = 0; i < 4; i++)
        cout << a[i][0] << "," << a[i][1] << "," << a[i][2] << "," << endl;
}

int main(int argc, char *argv[])
{
    sort2Darr();
    return 0;
}