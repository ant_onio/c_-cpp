#include "Lab5_call.h"
#include "Lab5_loadfile.h"
#include "Lab5_data.h"

//new
void ReadFile()
{
    int number;
    int **cont; //容器变量
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        if ((fp = fopen(path, "r")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fscanf(fp, "%d", &number); //得行数
        // cont = (int **)malloc(sizeof(int *) * number);
        cont = new int *; //行
        for (int i = 0; i < number; i++)
        {
            // cont[i] = (int *)malloc(sizeof(int) * 3); //3列
            cont[i] = new int;
            fscanf(fp, "%d,%d,%d\n", &cont[i][0], &cont[i][1], &cont[i][2]);
        }
    }
    else
    { //DAT
        if ((fp = fopen(path, "rb")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fread(&number, 4, 1, fp); //
        // cont = (int **)malloc(sizeof(int *) * number); //行
        cont = new int *; //行
        for (int i = 0; i < number; i++)
        {
            // cont[i] = (int *)malloc(sizeof(int) * 3); //3列
            cont[i] = new int;
            fread(cont[i], 4, 3, fp);
        }
    }
}
void save2Darr() {}
void saveStructArr() {}
void savePointerArr() {}
void saveLLarr() {}
//3~6 读数据
void read2Darr() //3
{
    int number;
    int **cont; //容器变量
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        if ((fp = fopen(path, "r")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fscanf(fp, "%d", &number); //得行数
        // cont = (int **)malloc(sizeof(int *) * number);
        cont = new int *; //行
        for (int i = 0; i < number; i++)
        {
            // cont[i] = (int *)malloc(sizeof(int) * 3); //3列
            cont[i] = new int;
            fscanf(fp, "%d,%d,%d\n", &cont[i][0], &cont[i][1], &cont[i][2]);
        }
    }
    else
    { //DAT
        if ((fp = fopen(path, "rb")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fread(&number, 4, 1, fp); //
        // cont = (int **)malloc(sizeof(int *) * number); //行
        cont = new int *; //行
        for (int i = 0; i < number; i++)
        {
            // cont[i] = (int *)malloc(sizeof(int) * 3); //3列
            cont[i] = new int;
            fread(cont[i], 4, 3, fp);
        }
    }
    //display
    printf("%d\n", number);
    for (int i = 0; i < number; i++)
        printf("%d,%d,%d\n", cont[i][0], cont[i][1], cont[i][2]);
    //past-process
    fclose(fp);
    // for (int i = 0; i < number; i++)
    //     free(cont[i]);
    delete[] cont; // free(cont)
    strcpy(notify, "二维数组方式读取文件");

    printf("\n按Enter键继续……");
    getchar();
}
void readStructArr() //4
{
    int number;
    DATAITEM *cont;
    //Path
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        if ((fp = fopen(path, "r")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fscanf(fp, "%d", &number);
        cont = (DATAITEM *)malloc(sizeof(DATAITEM) * number);
        for (int i = 0; i < number; i++)
        {
            fscanf(fp, "%d,%d,%d\n", &cont[i].item1, &cont[i].item2, &cont[i].item3);
        }
    }
    else
    { //DAT
        if ((fp = fopen(path, "rb")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fread(&number, 4, 1, fp); //
        cont = (DATAITEM *)malloc(sizeof(DATAITEM) * number);
        fread(cont, 4, number * 3, fp);
    }
    //display
    printf("%d\n", number);
    int i;
    for (i = 0; i < number; i++)
        printf("%d,%d,%d\n", cont[i].item1, cont[i].item2, cont[i].item3);
    //past-process
    fclose(fp);
    free(cont);
    strcpy(notify, "结构体数组方式读取文件");

    printf("\n按Enter键继续……");
    getchar();
}
// 5
void readPointerArr()
{
    int number;
    int *cont[3];
    for (int i = 0; i < 3; i++)
        // cont[i] = new (int);
        cont[i] = (int *)malloc(sizeof(int) * 3);
    //Path
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        if ((fp = fopen(path, "r")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fscanf(fp, "%d", &number);
        for (int i = 0; i < number; i++)
        {
            fscanf(fp, "%d,%d,%d\n", &cont[0][i], &cont[1][i], &cont[2][i]);
        }
    }
    else
    { //DAT
        if ((fp = fopen(path, "rb")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fread(&number, 4, 1, fp); //
        for (int i = 0; i < number; i++)
        {
            fread(&cont[0][i], 4, 1, fp);
            fread(&cont[1][i], 4, 1, fp);
            fread(&cont[2][i], 4, 1, fp);
        }
    }
    //display
    printf("%d\n", number);
    for (int i = 0; i < number; i++)
        printf("%d,%d,%d\n", cont[0][i], cont[1][i], cont[2][i]);
    //past-process
    fclose(fp);
    for (int i = 0; i < 1; i++)
        delete cont[i];
    strcpy(notify, "指针数组方式读取文件");

    printf("\n按Enter键继续……");
    getchar();
}
// 6
void readLLarr()
{
    int number;
    //Path
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        if ((fp = fopen(path, "r")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        // read file
        fscanf(fp, "%d", &number);
        Container *Node;
        // Node = new Container;
        Node = (Container *)malloc(sizeof(Container) * number);
        Container *Head = &Node[0];
        Container *current;
        for (int i = 0; i < number; i++)
        {
            fscanf(fp, "%d,%d,%d", &Node[i].item[0], &Node[i].item[1], &Node[i].item[2]);
            Node[i].pNext = &Node[i + 1]; //Get Linked
        }
        fclose(fp);
        // display
        printf("%d\n", number);
        current = Head;
        do
        {
            printf("%d,%d,%d\n", current->item[0], current->item[1], current->item[2]);
            current = current->pNext; //Point to next
        } while ((current->pNext)->pNext != NULL);
        delete Node;
    }
    else
    { //DAT
        if ((fp = fopen(path, "rb")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fread(&number, 4, 1, fp);
        Container *Node;
        // Node = new Container;
        Node = (Container *)malloc(sizeof(Container) * number);
        Container *Head = &Node[0];
        Container *current;
        for (int i = 0; i < number; i++)
        {
            for (int j = 0; j < 3; j++)
                fread(&Node[i].item[j], 4, 1, fp);
            Node[i].pNext = &Node[i + 1]; //Get Linked
        }
        fclose(fp);
        // display
        printf("%d\n", number);
        current = Head;
        do
        {
            printf("%d,%d,%d\n", current->item[0], current->item[1], current->item[2]);
            current = current->pNext; //Point to next
        } while ((current->pNext)->pNext != NULL);
        delete Node;
    }
    strcpy(notify, "链表方式读取文件");

    printf("\n按Enter键继续……");
    getchar();
}
//7~10 存文本数组函数
// 7
void save2DarrTXT()
{
    callFile('T');
    int number;
    int **cont; //容器变量
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        fp = fopen(path, "r");
        fscanf(fp, "%d", &number); //得行数
        // cont = (int **)malloc(sizeof(int *) * number); //行
        cont = new int *;
        for (int i = 0; i < number; i++)
        {
            // cont[i] = (int *)malloc(sizeof(int) * 3); //3列
            cont[i] = new int;
            fscanf(fp, "%d,%d,%d\n", &cont[i][0], &cont[i][1], &cont[i][2]);
        }
    }
    //display
    printf("%d\n", number);
    for (int i = 0; i < number; i++)
        printf("%d,%d,%d\n", cont[i][0], cont[i][1], cont[i][2]);
    //past-process
    fclose(fp);
    delete[] cont;
    printf("\n按Enter键继续……");
    getchar();

    strcpy(notify, "输出文本文件，二维数组数据");
}
// 8
void saveStructArrTXT()
{
    callFile('T');
    //read
    int number;
    DATAITEM *cont;
    // cont = (DATAITEM *)malloc(sizeof(DATAITEM) * conf.number);
    cont = new DATAITEM;
    //Path
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        fp = fopen(path, "r");
        fscanf(fp, "%d", &number);
        for (int i = 0; i < number; i++)
        {
            fscanf(fp, "%d,%d,%d\n", &cont[i].item1, &cont[i].item2, &cont[i].item3);
        }
    }
    //display
    printf("%d\n", number);
    int i;
    for (i = 0; i < number - 1; ++i)
        printf("%d,%d,%d\n", cont[i].item1, cont[i].item2, cont[i].item3);
    //past-process
    fclose(fp);
    // free(cont);
    delete cont;
    printf("\n按Enter键继续……");
    getchar();

    strcpy(notify, "输出文本文件，结构体数组数据");
}
// 9
void savePointerArrTXT()
{
    callFile('T');
    // read
    int number;
    int *cont[3];
    for (int i = 0; i < 3; i++)
        cont[i] = new (int);
    //Path
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        fp = fopen(path, "r");
        fscanf(fp, "%d", &number);
        for (int i = 0; i < number; i++)
        {
            fscanf(fp, "%d,%d,%d\n", &cont[0][i], &cont[1][i], &cont[2][i]);
        }
    }
    //display
    printf("%d\n", number);
    for (int i = 0; i < number; i++)
        printf("%d,%d,%d\n", cont[0][i], cont[1][i], cont[2][i]);
    //post-process
    fclose(fp);
    for (int i = 0; i < 1; i++)
        delete cont[i];
    printf("\n按Enter键继续……");
    getchar();

    strcpy(notify, "输出文本文件，指针数组数据");
}
//10
void saveLLarrTXT()
{
    callFile('T');
    int number;
    //Path
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".txt") != NULL)
    { //TXT
        if ((fp = fopen(path, "r")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        // read file
        fscanf(fp, "%d", &number);
        Container *Node;
        // Node = new Container;
        Node = (Container *)malloc(sizeof(Container) * number);
        Container *Head = &Node[0];
        Container *current;
        for (int i = 0; i < number; i++)
        {
            fscanf(fp, "%d,%d,%d", &Node[i].item[0], &Node[i].item[1], &Node[i].item[2]);
            Node[i].pNext = &Node[i + 1]; //Get Linked
        }
        fclose(fp);
        // display
        printf("%d\n", number);
        current = Head;
        do
        {
            printf("%d,%d,%d\n", current->item[0], current->item[1], current->item[2]);
            current = current->pNext; //Point to next
        } while ((current->pNext)->pNext != NULL);
        delete Node;
    }
    printf("\n按Enter键继续……");
    getchar();
    strcpy(notify, "输出文本文件，链表数据");
}
//11~14 存二进制数组函数
// 11
void save2DarrBIN()
{
    callFile('B');
    int number;
    int **cont; //容器变量
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".dat") != NULL)
    { //DAT
        fp = fopen(path, "rb");
        fread(&number, 4, 1, fp);                      //
        cont = (int **)malloc(sizeof(int *) * number); //行
        for (int i = 0; i < number; i++)
        {
            cont[i] = (int *)malloc(sizeof(int) * 3); //3列
            fread(cont[i], 4, 3, fp);
        }
        fclose(fp);
    }
    //display
    printf("%d\n", number);
    int i;
    for (i = 0; i < number; i++)
        printf("%d,%d,%d\n", cont[i][0], cont[i][1], cont[i][2]);
    //past-process

    for (int i = 0; i < number; i++)
        free(cont[i]);
    free(cont);
    printf("\n按Enter键继续……");
    getchar();

    strcpy(notify, "输出二进制文件，二维数组数据");
}
// 12
void saveStructArrBIN()
{
    callFile('B');

    int number;
    DATAITEM *cont;
    cont = (DATAITEM *)malloc(sizeof(DATAITEM) * conf.number);
    //Path
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".dat") != NULL)
    { //DAT
        fp = fopen(path, "rb");
        fread(&number, 4, 1, fp); //4位？
        fread(cont, 4, number * 3, fp);
        fclose(fp);
    }
    //display
    printf("%d\n", number);
    int i;
    for (i = 0; i < number; i++)
        printf("%d,%d,%d\n", cont[i].item1, cont[i].item2, cont[i].item3);
    //past-process
    free(cont);
    printf("\n按Enter键继续……");
    getchar();

    strcpy(notify, "输出二进制文件，结构体数组数据");
}
// 13
void savePointerArrBIN()
{
    callFile('B');

    int number;
    int *cont[3];
    for (int i = 0; i < 3; i++)
        cont[i] = new (int);
    //Path
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".dat") != NULL)

    { //DAT
        fp = fopen(path, "rb");
        fread(&number, 4, 1, fp); //
        for (int i = 0; i < number; i++)
        {
            fread(&cont[0][i], 4, 1, fp);
            fread(&cont[1][i], 4, 1, fp);
            fread(&cont[2][i], 4, 1, fp);
        }
    }
    //display
    printf("%d\n", number);
    for (int i = 0; i < number; i++)
        printf("%d,%d,%d\n", cont[0][i], cont[1][i], cont[2][i]);
    //past-process
    fclose(fp);
    for (int i = 0; i < 1; i++)
        delete cont[i];
    printf("\n按Enter键继续……");
    getchar();

    strcpy(notify, "输出二进制文件，存储指针数组数据");
}
// 14
void saveLLarrBIN()
{
    callFile('B');

    int number;
    //Path
    char path[MAX_STR_LEN] = {0};
    strcat(path, conf.filesavepath);
    strcat(path, "/");
    strcat(path, conf.filename);
    if (strstr(conf.filename, ".dat") != NULL)
    { //DAT
        if ((fp = fopen(path, "rb")) == NULL)
        {
            strcpy(notify, "文件打开失败，请检查路径或生成文件");
            return;
        }
        fread(&number, 4, 1, fp);
        Container *Node;
        // Node = new Container;
        Node = (Container *)malloc(sizeof(Container) * number);
        Container *Head = &Node[0];
        Container *current;
        for (int i = 0; i < number; i++)
        {
            for (int j = 0; j < 3; j++)
                fread(&Node[i].item[j], 4, 1, fp);
            Node[i].pNext = &Node[i + 1]; //Get Linked
        }
        fclose(fp);
        // display
        printf("%d\n", number);
        current = Head;
        do
        {
            printf("%d,%d,%d\n", current->item[0], current->item[1], current->item[2]);
            current = current->pNext; //Point to next
        } while ((current->pNext)->pNext != NULL);
        delete Node;
    }
    printf("\n按Enter键继续……");
    getchar();

    strcpy(notify, "输出二进制文件，存储链表数据");
}