// 程序主控文件，存放run函数及其他需要配套的子函数
#include "Lab5_main.h"
#include "Lab5_call.h"
#include "Lab5_view.h"
#include "Lab5_conf.h"
#include "Lab5_data.h"

char *notify; //提示信息
CONF conf = {0};
FILE *fp = NULL;

/*
*函数名称：Run
*函数功能：
*输入参数：int argc：命令行参数个数, char* argv[]:命令行参数值
*返回值：无
*版本信息：create by 董逸箫，2021-04-08
*/
void Run(int argc, char *argv[])
{
    /*/for test
    cout << "argc:" << argc << endl;
    for (int i = 0; i < argc; i++)
    {
        cout << "argv[" << i << "]:" << argv[i] << endl;
    } //*/

    //Init
    notify = (char *)malloc(64);
    // notify = new char;
    strcpy(notify, "Lab5");

    //Self-check
    switch (selfCheck())
    {
    case 0:
        puts("Mode File Missing!");
        return;
    case -1:
        puts("Config File Missing!");
        return;
    case -2:
        puts("Lab4 Program Missing!");
        return;
    case 1:
        break;
    default:
        puts("Selfcheck Failed!");
        return;
    }

    Init(argv[0]);

    // Preset Parameters by run mode
    // PresetPara_menu(mode, &conf);

    //MENU
    Main_menu();
}
/*
*函数名称：selfCheck
*函数功能：程序自检
*输入参数：
*返回值：
*版本信息：create by 董逸箫，2021-05-23
*/
int selfCheck()
{
    //工作模式文件 -0
    fp = fopen("./modeCONF.ini", "r");
    if (fp == NULL)
    {
        return 0;
    }
    else
    {
        fscanf(fp, "%d", &conf.runMode);
        if (conf.runMode != 0 && conf.runMode != 1)
        {
            fclose(fp);
            return 0;
        }
        fclose(fp);
    }
    fp = NULL;

    // 参数信息文件 (-1)
    fp = fopen("./conf.ini", "r");
    if (fp == NULL)
    {
        return -1;
    }
    fclose(fp);
    // Lab4程序 (-2)
    char buffer[MAX_STR_LEN];
    fp = popen("find ./Lab4", "r");
    fscanf(fp, "%s\n", buffer);
    if (strspn(buffer, "./Lab4") != strlen(buffer)) //不存在 Lab4
    {
        return -2;
    }
    return 1;
}

/*
*函数名称：Init
*函数功能：初始化程序
*输入参数：
*返回值：0自动模式；1交互模式
*版本信息：create by 董逸箫，2021-05-23
*/
void Init(char *programPath)
{
    fp = fopen("./modeCONF.ini", "r");
    if (fp != NULL)
    {
        fscanf(fp, "%d", &conf.runMode);
        printf("runMode:%d\n", conf.runMode);
        fclose(fp);
    }

    fp = fopen("./conf.ini", "r");
    if (fp != NULL)
    {
        fscanf(fp, "%s", conf.filesavepath);
        fscanf(fp, "%s", conf.filename);
        conf.number = -1;
        fscanf(fp, "%d", &conf.maxvalue1);
        fscanf(fp, "%d", &conf.minvalue1);
        fscanf(fp, "%d", &conf.maxvalue2);
        fscanf(fp, "%d", &conf.minvalue2);
        fscanf(fp, "%d", &conf.recordcount1);
        fscanf(fp, "%d", &conf.recordcount2);
    }
    fclose(fp);

    // FormatCONF(); //格式化配置数据

    if (conf.runMode == 0) //自动模式（初始化为默认文件名与路径）
    {
        // 获取Lab5绝对路径（传参）
        char *slash = strrchr(programPath, '/');
        *slash = '\0';

        // 获取shell当前cd路径 buffer
        // char buffer[MAX_STR_LEN];
        // fp = popen("pwd", "r");
        // fscanf(fp, "%s\n", buffer);
        // pclose(fp);

        strcpy(conf.filesavepath, programPath);
        strcat(conf.filesavepath, "/DataSet");
        strcpy(conf.filename, "DataFile.txt");
    }
    FormatCONF(); //格式化配置数据
}