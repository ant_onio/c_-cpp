//存放主控文件的函数说明及程序中主要结构体和全局变量声明
#ifndef _LAB5_MAIN__H
#define _LAB5_MAIN__H

#include <iostream>
#include <cstdio>
#include <stdio.h>
#include <cstdlib>
#include <string.h>
#include <ctime>

using namespace std;

//Pre-process
int selfCheck();
void Init(char *programPath);
//Run
void Run(int argc, char *argv[]);

#endif