#ifndef __LAB5_LOADFILE_H__
#define __LAB5_LOADFILE_H__

extern FILE *fp;

// new
void ReadFile(char *filetype);
void save2Darr();
void saveStructArr();
void savePointerArr();
void saveLLarr();

//3~6 读数据
void read2Darr();
void readStructArr();
void readPointerArr();
void readLLarr();
//7~10 存文本数组函数
void save2DarrTXT();
void saveStructArrTXT();
void savePointerArrTXT();
void saveLLarrTXT();
//11~14 存二进制数组函数
void save2DarrBIN();
void saveStructArrBIN();
void savePointerArrBIN();
void saveLLarrBIN();

#endif