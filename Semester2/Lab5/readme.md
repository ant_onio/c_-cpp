# Lab5 
## 文件结构
```
.
├── CMakeLists.txt
├── Lab4.cpp
├── Lab5.cpp
├── conf.ini
├── include
│   ├── Lab4_fun.h
│   ├── Lab4_run.h
│   ├── Lab5_call.h
│   ├── Lab5_conf.h
│   ├── Lab5_data.h
│   ├── Lab5_loadfile.h
│   ├── Lab5_main.h
│   └── Lab5_view.h
├── modeCONF.ini
├── readme.md
├── src
│   ├── Lab4_fun.cpp
│   ├── Lab4_run.cpp
│   ├── Lab5_call.cpp
│   ├── Lab5_conf.cpp
│   ├── Lab5_loadfile.cpp
│   ├── Lab5_main.cpp
│   └── Lab5_view.cpp
└── test.cpp
```
## 编译
1. 环境 Ubuntu 20.04.02 LTS, GCC 9.3.0, cmake 3.16.3  
2. 进入目录 `cd ./Lab5/build`  
   编译 `build ..`
3. 运行 `./Lab5`

# Issue 
## #1 2021/5/26
`Lab5_loadfile.cpp`的`save2DarrTXT()`函数中，该行运行时报错。
```
cont = (int **)malloc(sizeof(int *) * number); //行
```  
具体现象： 
执行功能8后正确回主菜单，再执行功能7后在该行报错，报错函数为malloc.c  
解决：换用C++语法 new/delete
## #2 2021/5/30
invalid size()  
解决：换回malloc()

# Change
1. (√)View中判断opt功能放到default中
2. (√)查看配置参数功能合并到二级菜单的实时显示功能中
3. (√) 自动模式下，数据记录文件的默认名称为DataFile.txt或DataFile.dat，存储在./DataSet子目录下。
4. 可替换：
```
// strcpy(buffer[1], conf.filename);
// strcat(buffer[1], "\n");
sprintf(buffer[0],"%s\n", conf.filesavepath);
```
5. 文件写单行
```
fscanf();
fseek(fp,0,1); // fp, 0偏置, 原位置
fprintf();
```
6. (√) CallFile函数  
可合并部分代码！不涉及打开二进制文件  

7. 需补充检查合法性  
获取文件名、获取文件路径  

8. 获取值函数可合并，传参三个（目标，最大值、最小值）  
  (√) getvalue
  (·) getchar

9. (√)argv[0]为 pwd 路径！ 可替换自动模式使用popen管道  

10. 报告中添加勘误修改