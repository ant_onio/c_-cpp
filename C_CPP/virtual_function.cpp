/*
** 参考 如下说明
** https://blog.csdn.net/smstong/article/details/50669732
** 把这个例子编译连接 运行通过，理解
** 用c++语言重新虚函数。
*/

#include <iostream>
using namespace std;

class Animal
{ // base class
    static int PID;

public:
    Animal()
    {
        PID += 1;
        std::cout << "PID:" << PID << ", Animal" << std::endl;
    }
    ~Animal() { PID -= 1; }
    virtual void eat() { cout << "Animal::eat()" << endl; }
    virtual void bite() { cout << "Animal::bite()" << endl; }
    virtual void speak() { cout << "Animal::speak()" << endl; }
};
int Animal::PID = 0;

class Dog : public Animal
{ // son class
public:
    void eat() { cout << "Dog::eat()" << endl; }
    void bite() { cout << "Dog::bite()" << endl; }
};

class Cat : public Animal
{ // son class
public:
    void eat() { cout << "Cat::eat()" << endl; }
    void bite() { cout << "Cat::bite()" << endl; }
};

int main()
{
    Dog *pdog = new Dog;
    // Dog dog;
    Cat *pcat = new Cat;
    //
    Animal *animal1 = (Animal *)pdog;
    // Animal *animal1 = &dog;
    Animal *animal2 = (Animal *)pcat;
    //
    animal1->eat();
    animal1->bite();
    animal1->speak();

    animal2->eat();
    animal2->bite();
    animal2->speak();

    return 0;
}