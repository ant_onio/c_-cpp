#include <iostream>
#include <vector>
using namespace std;

class Polygon
{
    static int PID;

protected:
    int numVertices;
    vector<float> xCoord, yCoord;

public:
    Polygon()
    {
        PID += 1;
        cout << "Polygon Generated: ID = " << PID << endl;
    }
    Polygon(const Polygon &obj) // 拷贝构造函数
    {
        PID += 1;
        cout << "Polygon Generated: ID = " << PID << endl;
        cout << "Poly Copy function" << endl;
        this->numVertices = obj.numVertices;
        this->xCoord.clear();
        this->yCoord.clear();
        for (int i = 0; i < numVertices; i++)
        {
            this->xCoord.push_back(obj.xCoord[i]);
            this->yCoord.push_back(obj.yCoord[i]);
        }
    }
    void set(vector<float> &x, vector<float> &y, int nV)
    {
        numVertices = nV;
        xCoord.clear();
        yCoord.clear();
        for (int i = 0; i < numVertices; i++)
        {
            xCoord.push_back(x[i]);
            yCoord.push_back(y[i]);
        }
    }
    Polygon &operator=(const Polygon &obj)
    {
        cout << "'=' Reload" << endl;
        if (this == &obj)
            return *this;
        this->numVertices = obj.numVertices;
        this->xCoord.clear();
        this->yCoord.clear();
        for (int i = 0; i < this->numVertices; i++)
        {
            this->xCoord.push_back(obj.xCoord[i]);
            this->yCoord.push_back(obj.yCoord[i]);
        }
        return *this;
    }
    ~Polygon()
    {
        cout << "Polygon Deleted: ID = " << PID << endl;
        PID -= 1;
    }
};
int Polygon::PID = 0;

class Rectangle : public Polygon
{
public:
    Rectangle()
    {
        cout << "Rectangle Generated" << endl;
    }
    Rectangle(const Rectangle &obj) : Polygon(obj) // 拷贝构造函数，调用基类的
    {
        cout << "Rectancle Copy function" << endl;
    }
    float area()
    {
        int j;
        double area = 0;
        for (int i = 0; i < numVertices; i++)
        {
            j = (i + 1) % numVertices;
            area += this->xCoord[i] * this->yCoord[j];
            area -= this->yCoord[i] * this->xCoord[j];
        }
        area /= 2;
        return (area < 0 ? -area : area);
    }
    Rectangle &operator=(const Rectangle &R) /*: Polygon(R)*/
    {
        cout << "'=' Reload" << endl;
        if (this == &R)
            return *this;
        this->numVertices = R.numVertices;
        this->xCoord.clear();
        this->yCoord.clear();
        for (int i = 0; i < this->numVertices; i++)
        {
            this->xCoord.push_back(R.xCoord[i]);
            this->yCoord.push_back(R.yCoord[i]);
        }
        return *this;
    }
    ~Rectangle()
    {
        cout << "Rectangle Deleted" << endl;
    }
};

int main()
{
    // freopen("../test.txt", "r", stdin);
    // init
    int nV = 0;
    int tempX, tempY;
    vector<float> x;
    vector<float> y;
    cin >> nV;
    for (int i = 0; i < nV; i++)
    {
        cin >> tempX >> tempY;
        x.push_back(tempX);
        y.push_back(tempY);
    }

    Rectangle A1;
    cout << endl;
    A1.set(x, y, nV);

    Rectangle A2(A1); // 拷贝构造函数，
    cout << endl;

    Rectangle A3;
    A3 = A1; // 等号赋值函数
    cout << endl;

    cout << "area: " << A3.area() << endl;
    cout << endl;

    return 0;
}