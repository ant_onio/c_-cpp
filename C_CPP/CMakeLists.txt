cmake_minimum_required(VERSION 3.0)

project(C_CPP)

SET(CMAKE_BUILD_TYPE "Debug")
SET(CMAKE_CXX_FLAGS_DEBUG "$ENV{CXXFLAGS} -O0 -Wall -g2 -ggdb")  
SET(CMAKE_CXX_FLAGS_RELEASE "$ENV{CXXFLAGS} -O3 -Wall")  


include_directories(${CMAKE_SOURCE_DIR}/include)
link_directories(./lib)

add_compile_options(-Wall -std=c++11)

add_executable(FileRW FileRW.cpp)
# add_executable(map Map.cpp)
