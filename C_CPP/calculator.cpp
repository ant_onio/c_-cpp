// 利用interpreter模式，实现 计算表达式的计算。
// 例如 ((2+3)*5+8)+6

#include <bits/stdc++.h>
using namespace std;

class expression
{
public:
    virtual double value() = 0;
    char ch;
    expression *left = NULL;
    expression *right = NULL;
};
class Const : public expression
{
public:
    double i = 0;
    Const(double ii) { i = ii; }
    double value() { return i; }
};
class add : public expression
{
public:
    double value() { return left->value() + right->value(); }
};
class minus : public expression
{
public:
    double value() { return left->value() - right->value(); }
};
class multi : public expression
{
public:
    double value() { return left->value() * right->value(); }
};
class div : public expression
{
public:
    double value() { return left->value() / right->value(); }
};

int main()
{
    string Expression("((2+3)*5+8)+6");
    // exp1 *root;
    expression *a = new Const(2);
    expression *b = new add();
    expression *c = new Const(3);
    expression *d = new multi();
    expression *e = new Const(5);
    expression *f = new add();
    expression *g = new Const(8);
    expression *h = new add();
    expression *i = new Const(6);
    h->right = i;
    h->left = f;
    f->right = g;
    f->left = d;
    d->right = e;
    d->left = b;
    b->right = c;
    b->left = a;
    cout << h->value() << endl;

    return 0;
}