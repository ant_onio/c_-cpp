/*
 *柱体类（虚函数） 
 *用虚函数写一个柱体类，类包含Geometry *pbottom;double height;两个变量，写函数求柱体的体积，用不同的底继承
**/

#include <iostream>
#include <math.h>
using namespace std;

class Geometry
{
public:
    virtual double getArea() = 0;
};

class Pillar
{
public:
    Geometry *pbottom;
    double height;
    Pillar()
    {
        height = 5;
    }
    Pillar(Geometry *p, double HEIGHT0)
    {

        pbottom = p;
        height = HEIGHT0;
    }
    double getVolume()
    {
        return pbottom->getArea() * height;
    }
};

class circle : public Geometry
{
    double getArea()
    {
        return 3.14 * r * r;
    }

public:
    double r;
    circle(double rr)
    {
        r = rr;
    }
};

class triangle : public Geometry
{
    double getArea()
    {
        return 0.25 * sqrt((a + b + c) * (a + b - c) * (a + c - b) * (b + c - a));
    }

public:
    double a, b, c;
    triangle(double aa, double bb, double cc)
    {
        a = aa;
        b = bb;
        c = cc;
    }
};

int main()
{
    double ans1, ans2;
    circle A(2.5);
    triangle B(2, 3, 2);

    Pillar D(&A, 5);
    ans1 = D.getVolume();
    cout << "Volume1: " << ans1 << endl;

    Pillar E(&B, 10);
    ans2 = E.getVolume();
    cout << "Volume2: " << ans2 << endl;
}