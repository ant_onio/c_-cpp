#include <vector>
#include <numeric>
#include <functional>
#include <iostream>
using namespace std;
vector<int> v1;
typedef struct node
{
	int data;
	struct node *lchild, *rclild;
} node, *tree;
void createtree(tree &T)
{
	int ch;
	cin >> ch;
	if (ch == 0)
		T = NULL;
	else
	{
		T = new node;
		T->data = ch;
		createtree(T->lchild);
		createtree(T->rclild);
	}
}
void preorder(tree T)
{
	if (T)
	{
		v1.push_back(T->data);
		preorder(T->lchild);
		preorder(T->rclild);
	}
}
int main()
{
	tree T;
	createtree(T);
	preorder(T);
	vector<int>::iterator Iter1, Iter2;
	cout << "最初向量v1中元素的值为：";
	for (Iter1 = v1.begin(); Iter1 != v1.end(); Iter1++)
		cout << *Iter1 << " ";
	cout << endl;
	//求和
	int total;
	total = accumulate(v1.begin(), v1.end(), 0);
	cout << total << endl;
	return 0;
}
