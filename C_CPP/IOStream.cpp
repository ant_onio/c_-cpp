#include <bits/stdc++.h>
using namespace std;

class MyString
{
public:
    string m_pbuf = "hello";
    void set_string(string str) {}
};

ostream &operator<<(ostream &o, const MyString &str)
{
    o << str.m_pbuf;
    return o;
}

//test


int main()
{
    // freopen("../data1.out", "w", stdout);

    int a = 10830;
    // cin >> a; // istream对象，遇空白（回车、换行、空格）停止
    cout << a << endl;

    // 重载 <<
    MyString str;
    str.set_string("Hello");
    cout << str << endl;

    //文件
    FILE *fp;
    fp = fopen("../ dataOut.bin", "wb+");
    // fprintf(fp, "%d", a);
    fwrite(&a, 4, 1, fp);
    fclose(fp);

    return 0;
}