#include <bits/stdc++.h>
using namespace std;

#define strSize 10
struct Data
{
    char name[strSize];
    int score;
};

int cmpStruct(const void *a, const void *b)
{
    Data *pa = (Data *)a;
    Data *pb = (Data *)b;
    return ((*pb).score - (*pa).score);
}

int main()
{
    FILE *fp;
    char str1[strSize], str2[strSize];
    Data D[6];
    Data D1[6];

    // read txt
    fp = fopen("../a.txt", "r");
    fscanf(fp, "%s", str1);
    fscanf(fp, "%s", str2);
    for (int i = 0; i < 6; i++)
        fscanf(fp, "%s%d", D[i].name, &D[i].score);
    fclose(fp);

    qsort(D, 6, sizeof(Data), cmpStruct);


    // binary File
    fp = fopen("../a.dat", "wb");
    fwrite(str1, strSize * sizeof(char), 1, fp);
    fwrite(str2, strSize * sizeof(char), 1, fp);
    for (int i = 0; i < 6; i++)
    {
        fwrite(D[i].name, strSize * sizeof(char), 1, fp);
        fwrite(&D[i].score, sizeof(int), 1, fp);
    }
    fclose(fp);

    fp = fopen("../a.dat", "rb");
    fread(str1, strSize, 1, fp);
    fread(str2, strSize, 1, fp);
    for (int i = 0; i < 6; i++)
    {
        fread(D1[i].name, strSize, 1, fp);
        fread(&D1[i].score, sizeof(int), 1, fp);
    }
    fclose(fp);

    D1[2].score += 20;
    fp = fopen("../a.dat", "wb+");
    fwrite(str1, strSize * sizeof(char), 1, fp);
    fwrite(str2, strSize * sizeof(char), 1, fp);
    for (int i = 0; i < 6; i++)
    {
        fwrite(D1[i].name, strSize * sizeof(char), 1, fp);
        fwrite(&D1[i].score, sizeof(int), 1, fp);
    }
    fclose(fp);

    cout << str1 << " " << str2 << endl;
    for (int i = 0; i < 6; i++)
        cout << D1[i].name << " " << D1[i].score << endl;

    return 0;
}