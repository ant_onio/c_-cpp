#include <iostream>
#include <vector>
using namespace std;

typedef struct Stu
{
    int grade;
    int id;
    Stu(int g) : grade(g), id(0){};
    Stu(int g, int gg) : grade(g), id(gg){};
    bool operator>=(Stu &input) const { return grade >= input.grade; }
} STU;

template <class T>
class Functor
{
private:
    T limit;

public:
    Functor(T l) : limit(l){};
    bool operator()(T x) { return x >= limit; }
};

class SCHOOL
{
public:
    int student[2];
    int techer[2];

    SCHOOL()
    {
        student[0] = 10;
        student[1] = 9;
        techer[0] = 11;
        techer[1] = 20;
    }

    class iterator
    {
    public:
        SCHOOL *p;
        int num;
        iterator(int n, SCHOOL *q) : num(n), p(q){};

        int operator*()
        {
            if (this->num < 20)
                return p->student[num];
            else
                return p->techer[num];
        }
        bool operator!=(iterator a)
        {
            if ((a.p != p) || (a.num != num))
                return 1;
            else
                return 0;
        }
        iterator operator++(int)
        {
            num++;
            return *this;
        }
    };
    iterator beginning()
    {
        int num = 0;
        return iterator(num, this);
    }
    iterator endidng()
    {
        int num = 4;
        return iterator(num, this);
    }
};

template <class T, class ff>
int acclmulate(T r1, T r2, Functor<ff> f)
{
    int sum = 0;
    for (T i = r1; i != r2; i++)
    {
        if (f(*(i)))
            sum += *i;
    }
    return sum;
}

int main()
{
    SCHOOL a;
    Functor<int> f(10);
    int ans;

    ans = acclmulate(a.beginning(), a.endidng(), f);
    cout << ans << endl;
    return 0;
}