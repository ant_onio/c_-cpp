#include <iostream>
#include <cstring>
using namespace std;

class CStudent;
class CSelectedCourse;
class CCourse;

class CStudent
{
public:
    string name;
    int ID;
    friend class CSelectCourse;
    CStudent() {}
    CStudent(string sName)
    {
        name = sName;
    }
    void showname()
    {
        cout << name << endl;
    }

    CStudent *next = NULL;
};

class CCourse
{
public:
    string name; // course name
    CStudent *stu = new CStudent("NULL");
    friend class CSelectCourse;
    CCourse() {}
    CCourse(string cName)
    {
        name = cName;
    }
    void listStudent(string name)
    {
    }
    CCourse *next = NULL;
};

class CSelectCourse
{
public:
    int ID = 0;
    CStudent *pstu = NULL; // pointer student
    CCourse *pcou = NULL;

    CSelectCourse() {}
    CSelectCourse(string StuName, string CouName, CStudent *&stu, CCourse *&cou)
    {
        CStudent *curStu = stu->next;
        CCourse *curCou = cou->next;
        while (curStu->name != StuName)
        {
            if (curStu->next == NULL)
            {
                //error
                break;
            }
            curStu = curStu->next;
        }
        pstu = curStu;
        while (curCou->name != CouName)
        {
            if (curCou->next == NULL)
            {
                //error
                break;
            }
            curCou = curCou->next;
        }
        pcou = curCou;
        cout << "Link: " << pstu->name << ", " << pcou->name << endl;
    }
    void queryStudent(string CouName, CCourse *&cou) // query student of a course in a course group
    {
        CSelectCourse *curSel = this->next;
        cout << "Course: " << CouName << endl;
        while (curSel != NULL)
        {
            if (curSel->pcou->name == CouName)
                cout << curSel->pstu->name << ", ";
            curSel = curSel->next;
        }
        cout << endl;
    }
    void queryCourse(string StuName, CStudent *&stu)
    {
        CSelectCourse *curSel = this->next;
        cout << "Student: " << StuName << endl;
        while (curSel != NULL)
        {
            if (curSel->pstu->name == StuName)
                cout << curSel->pcou->name << ", ";
            curSel = curSel->next;
        }
        cout << endl;
    }

    CStudent *student;
    CCourse *course;
    CSelectCourse *next = NULL;
};

void StuInsert(CStudent *&head, string name)
{
    CStudent *cur = head;
    while (cur->next != NULL)
        cur = cur->next;
    CStudent *e = new CStudent(name);
    cur->next = e;
}

void CouInsert(CCourse *&head, string name)
{
    CCourse *cur = head;
    while (cur->next != NULL)
        cur = cur->next;
    CCourse *e = new CCourse(name);
    cur->next = e;
}
void LogInsert(CSelectCourse *&head, string StuName, string CouName, CStudent *&stu, CCourse *&cou)
{
    CSelectCourse *cur = head;
    while (cur->next != NULL)
        cur = cur->next;
    CSelectCourse *e = new CSelectCourse(StuName, CouName, stu, cou);
    cur->next = e;
}
int main(int argc, char *argv[])
{
    // Init
    CCourse *cou = new CCourse;
    CouInsert(cou, "Math");
    CouInsert(cou, "English");
    CouInsert(cou, "Physics");
    CStudent *stu = new CStudent;
    StuInsert(stu, "ZhangSan");
    StuInsert(stu, "LiSi");
    StuInsert(stu, "WangWu");

    // Select Course
    CSelectCourse *log = new CSelectCourse;
    LogInsert(log, "LiSi", "Math", stu, cou);
    LogInsert(log, "LiSi", "English", stu, cou);
    LogInsert(log, "WangWu", "English", stu, cou);
    LogInsert(log, "ZhangSan", "Math", stu, cou);

    // Query
    log->queryStudent("Math", cou);
    log->queryCourse("LiSi", stu);

    return 0;
}