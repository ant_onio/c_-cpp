#include <iostream>
using namespace std;

void swap1(char *&a, char *&b)
{
    cout << "swap1: " << a << ", " << b << endl;
    char *t;
    t = a;
    a = b;
    b = t;
} //引用

void swap2(char **a, char **b)
{
    cout << "swap2:" << a << ", " << b << endl;
    char *t;
    t = *a;
    *a = *b;
    *b = t;
} // 指针

int main(int argc, char *argv[])
{
    char p[] = "hello";
    char q[] = "world";
    char *x = p, *y = q;

    cout << "before: " << x << ", " << y << endl;
    swap1(x, y);
    cout << "after: " << x << ", " << y << endl;
    cout << endl;

    cout << "before: " << x << ", " << y << endl;
    swap2(&x, &y);
    cout << "after: " << x << ", " << y << endl;

    return 0;
}