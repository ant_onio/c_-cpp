/*
** 实现一个二叉树的迭代器，并调用 stl中 的accumulate函数测试。
** accumulate的使用范例参考下面的链接
** https://blog.csdn.net/u011499425/article/details/52756242
*/

#include <bits/stdc++.h>
using namespace std;
vector<int> v1;

struct node
{
    int val;
    node *lchild = NULL;
    node *rchild = NULL;
};

void Insert(node *&P, int e)
{
}

void CreateTree(node *&T)
{
    int ch;
    cin >> ch;
    if (ch == 0)
        T = NULL;
    else
    {
        T = new node;
        T->val = ch;
        CreateTree(T->lchild);
        CreateTree(T->rchild);
    }
}
void PreOrder(node *&T)
{
    if (T)
    {
        v1.push_back(T->val);
        PreOrder(T->lchild);
        PreOrder(T->rchild);
    }
}

int main()
{
    node *root;
    CreateTree(root);
    PreOrder(root);

    vector<int>::iterator Iter1, Iter2;
    for (Iter1 = v1.begin(); Iter1 != v1.end(); Iter1++)
        cout << *Iter1 << " ";
    cout << endl;

    int sum = accumulate(v1.begin(), v1.end(), 0);
    cout << sum << endl;

    return 0;
}