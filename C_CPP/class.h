#ifndef _CLASS_H_
#define _CLASS_H_

// #include <cstring>

class CStudent;
class CSelectedCourse;
class CCourse;

class CStudent
{
public:
    string name;
    int ID;
    friend class CSelectCourse;
    CStudent() {}
    CStudent(string sName)
    {
        name = sName;
    }
    void showname()
    {
        cout << name << endl;
    }

    CStudent *next = NULL;
};

class CCourse
{
public:
    string name; // course name
    CStudent *stu = new CStudent("NULL");
    friend class CSelectCourse;
    CCourse() {}
    CCourse(string cName)
    {
        name = cName;
    }
    void listStudent(string name)
    {
    }
    CCourse *next = NULL;
};

class CSelectCourse
{
public:
    int ID = 0;
    CStudent *pstu = NULL; // pointer student
    CCourse *pcou = NULL;

    CSelectCourse() {}
    CSelectCourse(string StuName, string CouName, CStudent *&stu, CCourse *&cou)
    {
        CStudent *curStu = stu->next;
        CCourse *curCou = cou->next;
        while (curStu->name != StuName)
        {
            if (curStu->next == NULL)
            {
                //error
                break;
            }
            curStu = curStu->next;
        }
        pstu = curStu;
        while (curCou->name != CouName)
        {
            if (curCou->next == NULL)
            {
                //error
                break;
            }
            curCou = curCou->next;
        }
        pcou = curCou;
        cout << "Link: " << pstu->name << ", " << pcou->name << endl;
    }
    void queryStudent(string CouName, CCourse *&cou) // query student of a course in a course group
    {
        CSelectCourse *curSel = this->next;
        cout << "Course: " << CouName << endl;
        while (curSel != NULL)
        {
            if (curSel->pcou->name == CouName)
                cout << curSel->pstu->name << ", ";
            curSel = curSel->next;
        }
        cout<<endl;
    }
    void queryCourse(string StuName, CStudent *&stu)
    {
        CSelectCourse *curSel = this->next;
        cout << "Student: " << StuName << endl;
        while (curSel != NULL)
        {
            if (curSel->pstu->name == StuName)
                cout << curSel->pcou->name << ", ";
            curSel = curSel->next;
        }
        cout<<endl;
    }

    CStudent *student;
    CCourse *course;
    CSelectCourse *next = NULL;
};

#endif